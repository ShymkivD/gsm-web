import { LatLng } from 'leaflet';
import path from 'ramda/es/path';
import { call, delay, put, race, select, takeEvery, takeLatest, takeLeading } from 'redux-saga/effects';
import { v4 as uuid } from 'uuid';

import { Tabs } from '../../constants/dialogs';
import { MainMap } from '../../constants/map';
import { modes } from '../../constants/modes';
import { checkOSRMService } from '../../utils/api';
import { getRandomColor } from '../../utils/dom';
import { downloadGPXTrack, getGPXString, importGpxTrack } from '../../utils/gpx';
import { getUrlData, pushPath } from '../../utils/history';
import { OsrmRouter } from '../../utils/map/OsrmRouter';
import { Unwrap } from '../../utils/middleware';
import {
  composeArrows,
  composeDistMark,
  composeImages,
  composePoly,
  downloadCanvas,
  fetchImages,
  getPolyPlacement,
  getTilePlacement,
  imageFetcher,
} from '../../utils/renderer';
import { simplify } from '../../utils/simplify';
import { mapClicked, mapSet, mapSetRoute } from '../map/actions';
import { mapActions } from '../map/constants';
import { loadMapFromPath } from '../map/sagas';
import { selectMap, selectMapRoute } from '../map/selectors';
import { searchSetTab } from '../user/actions';
import { selectUser } from '../user/selectors';
import { editorInitialState } from '.';
import * as EditorActions from './actions';
import { editorActions, editorHistoryLength } from './constants';
import { selectEditor, selectEditorGpx, selectEditorMode } from './selectors';

const hideLoader = () => {
  document.getElementById('loader').style.opacity = String(0);
  document.getElementById('loader').style.pointerEvents = 'none';

  return true;
};

function* stopEditingSaga() {
  const { changed, mode }: ReturnType<typeof selectEditor> = yield select(selectEditor);
  const { path } = getUrlData();

  if (changed && mode !== modes.CONFIRM_CANCEL) {
    yield put(EditorActions.changeMode(modes.CONFIRM_CANCEL));
    return;
  }

  yield put(EditorActions.changeMode(modes.NONE));
  yield put(EditorActions.setChanged(false));
  yield put(EditorActions.setReady(true));

  yield pushPath(`/${path}/`);
}

function* checkOSRMServiceSaga() {
  const routing = yield call(checkOSRMService, [new LatLng(1, 1), new LatLng(2, 2)]);

  yield put(EditorActions.setFeature({ routing }));
}

export function* setReadySaga() {
  yield put(EditorActions.setReady(true));
  hideLoader();

  yield call(checkOSRMServiceSaga);
  yield put(searchSetTab(Tabs.MY));
}

function* getRenderData() {
  yield put(EditorActions.setRenderer({ info: 'Loading tails', progress: 0.1 }));

  const { route, provider }: ReturnType<typeof selectMap> = yield select(selectMap);
  const { distance }: ReturnType<typeof selectEditor> = yield select(selectEditor);

  const canvas = <HTMLCanvasElement>document.getElementById('renderer');
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  const ctx = canvas.getContext('2d');

  const geometry = getTilePlacement();
  const points = getPolyPlacement(route);

  ctx.clearRect(0, 0, canvas.width, canvas.height);

  const images = yield fetchImages(ctx, geometry, provider);

  yield put(EditorActions.setRenderer({ info: 'Drawing', progress: 0.5 }));

  yield composeImages({ geometry, images, ctx });
  yield composePoly({ points, ctx });

  yield composeArrows({ points, ctx });
  yield composeDistMark({ ctx, points, distance });

  yield put(EditorActions.setRenderer({ info: 'Done', progress: 1 }));

  return yield canvas.toDataURL('image/jpeg');
}

function* takeAShotSaga() {
  const worker = call(getRenderData);

  const { result, timeout } = yield race({
    result: worker,
    timeout: delay(500),
  });

  if (timeout) {
    yield put(EditorActions.changeMode(modes.SHOT_PREFETCH));
  }

  const data = yield result || worker;

  yield put(EditorActions.changeMode(modes.NONE));
  yield put(
    EditorActions.setRenderer({
      data,
      renderer_active: true,
      width: window.innerWidth,
      height: window.innerHeight,
    }),
  );
}

function* getCropData({ x, y, width, height }) {
  const {
    renderer: { data },
  }: ReturnType<typeof selectEditor> = yield select(selectEditor);

  const canvas = <HTMLCanvasElement>document.getElementById('renderer');
  canvas.width = width;
  canvas.height = height;
  const ctx = canvas.getContext('2d');
  const image = yield imageFetcher(data);

  ctx.drawImage(image, -x, -y);

  return yield canvas.toDataURL('image/jpeg');
}

function* cropAShotSaga(params) {
  const { title, address }: ReturnType<typeof selectMap> = yield select(selectMap);

  yield call(getCropData, params);
  const canvas = document.getElementById('renderer') as HTMLCanvasElement;

  downloadCanvas(canvas, (title || address).replace(/\./gi, ' '));

  return yield put(EditorActions.hideRenderer());
}

function* locationChangeSaga() {
  const { ready }: ReturnType<typeof selectEditor> = yield select(selectEditor);

  if (!ready) {
    return;
  }

  const mode: ReturnType<typeof selectEditorMode> = yield select(selectEditorMode);

  if (mode !== modes.NONE) {
    yield put(EditorActions.changeMode(modes.NONE));
  }

  yield call(loadMapFromPath);
  MainMap.fitVisibleBounds({ animate: true });
}

function* keyPressedSaga({ key, target }: ReturnType<typeof EditorActions.keyPressed>) {
  if (target === 'INPUT' || target === 'TEXTAREA') {
    return;
  }

  if (key === 'Escape') {
    const {
      dialog_active,
      mode,
      renderer: { renderer_active },
    }: ReturnType<typeof selectEditor> = yield select(selectEditor);

    if (renderer_active) {
      return yield put(EditorActions.hideRenderer());
    }
    if (dialog_active) {
      return yield put(EditorActions.setDialogActive(false));
    }
    if (mode !== modes.NONE) {
      return yield put(EditorActions.changeMode(modes.NONE));
    }
  } else if (key === 'Delete' || key === 'c') {
    const { editing } = yield select(selectEditor);

    if (!editing) {
      return;
    }

    const { mode } = yield select(selectUser);

    if (mode === modes.TRASH) {
      yield put(EditorActions.clearAll());
    } else {
      yield put(EditorActions.changeMode(modes.TRASH));
    }
  } else if (key === 'z') {
    yield put(EditorActions.undo());
  } else if (key === 'x') {
    yield put(EditorActions.redo());
  }
}

function* getGPXTrackSaga() {
  const { route, title, address }: ReturnType<typeof selectMap> = yield select(selectMap);

  if (!route.length) {
    return;
  }

  const track = getGPXString({ route, title: title || address });

  return downloadGPXTrack({ track, title });
}

function* routerCancel() {
  yield put(EditorActions.changeMode(modes.NONE));
}

function* mapClick({ latlng }: ReturnType<typeof mapClicked>) {
  const { mode }: ReturnType<typeof selectEditor> = yield select(selectEditor);

  if (mode === modes.ROUTER) {
    const wp = OsrmRouter.getWaypoints().filter((point) => !!point.latLng);
    OsrmRouter.setWaypoints([...wp, latlng]);
  }
}

function* routerSubmit() {
  const route: ReturnType<typeof selectMapRoute> = yield select(selectMapRoute);
  const latlngs: LatLng[] = path(['_routes', 0, 'coordinates'], OsrmRouter);

  const coordinates = simplify(latlngs);

  yield put(mapSetRoute([...route, ...coordinates]));
  OsrmRouter.setWaypoints([]);
  yield put(EditorActions.changeMode(modes.NONE));
}

function* cancelSave() {
  yield put(
    EditorActions.setSave({
      ...editorInitialState.save,
    }),
  );
}

function* changeMode({ mode }: ReturnType<typeof EditorActions.changeMode>) {
  const current: ReturnType<typeof selectEditorMode> = yield select(selectEditorMode);

  if (mode === current) {
    yield put(EditorActions.setMode(modes.NONE));
    return;
  }

  switch (current) {
    case modes.ROUTER:
      yield put(EditorActions.setRouter({ waypoints: [] }));
  }

  yield put(EditorActions.setMode(mode));

  switch (mode) {
  }
}

function* changeHistory() {
  const { history }: ReturnType<typeof selectEditor> = yield select(selectEditor);
  const { route }: ReturnType<typeof selectMap> = yield select(selectMap);

  const current =
    history.records.length - 1 > history.position ? history.records.slice(0, history.position + 1) : history.records;

  const records = [...current, { route }];
  const min = Math.max(0, records.length - editorHistoryLength - 1);
  const max = Math.min(records.length, editorHistoryLength + 2);

  yield put(
    EditorActions.setHistory({
      records: records.slice(min, max),
      position: records.slice(min, max).length - 1,
    }),
  );
}

function* undoHistory() {
  const { history }: ReturnType<typeof selectEditor> = yield select(selectEditor);

  if (history.position === 0 || history.records.length <= 1) {
    return;
  }

  const { route } = history.records[history.position - 1];

  yield put(mapSet({ route }));
  yield put(EditorActions.setHistory({ position: history.position - 1 }));
}

function* redoHistory() {
  const { history }: ReturnType<typeof selectEditor> = yield select(selectEditor);

  if (history.position >= history.records.length - 1) {
    return;
  }

  const { route } = history.records[history.position + 1];

  yield put(mapSet({ route }));
  yield put(EditorActions.setHistory({ position: history.position + 1 }));
}

function* uploadGpx({ file }: ReturnType<typeof EditorActions.uploadGpx>) {
  const gpx: Unwrap<typeof importGpxTrack> = yield importGpxTrack(file);

  if (!gpx || !gpx.length) {
    return;
  }

  const { list }: ReturnType<typeof selectEditorGpx> = yield select(selectEditorGpx);

  yield put(
    EditorActions.setGpx({
      list: [
        ...list,
        ...gpx
          .filter((item) => item.latlngs && item.latlngs.length)
          .map((item) => ({
            enabled: true,
            latlngs: item.latlngs,
            color: getRandomColor(),
            name: item.name || `Track #${list.length + 1}`,
            id: uuid() as string,
          })),
      ],
    }),
  );
}

export function* editorSaga() {
  yield takeEvery(editorActions.LOCATION_CHANGED, locationChangeSaga);

  yield takeEvery(editorActions.STOP_EDITING, stopEditingSaga);
  yield takeLatest(editorActions.TAKE_A_SHOT, takeAShotSaga);
  yield takeLatest(editorActions.CROP_A_SHOT, cropAShotSaga);
  yield takeLatest(editorActions.KEY_PRESSED, keyPressedSaga);
  yield takeLatest(editorActions.GET_GPX_TRACK, getGPXTrackSaga);
  yield takeLatest(editorActions.ROUTER_CANCEL, routerCancel);
  yield takeLatest(mapActions.MAP_CLICKED, mapClick);
  yield takeLatest(editorActions.ROUTER_SUBMIT, routerSubmit);
  yield takeLatest(editorActions.CANCEL_SAVE, cancelSave);
  yield takeLeading(editorActions.CHANGE_MODE, changeMode);

  yield takeEvery([mapActions.SET_ROUTE, editorActions.CAPTURE_HISTORY], changeHistory);

  yield takeEvery(editorActions.UNDO, undoHistory);
  yield takeEvery(editorActions.REDO, redoHistory);

  yield takeEvery(editorActions.UPLOAD_GPX, uploadGpx);
}
