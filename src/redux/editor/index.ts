import { LatLng } from 'leaflet';

import { Dialogs } from '../../constants/dialogs';
import { modes } from '../../constants/modes';
import { createReducer } from '../../utils/reducer';
import { MapReducer } from '../map';
import { drawingDirections } from './constants';
import { editorHandlers } from './handlers';

export interface GpxRoute {
  latlngs: LatLng[];
  enabled: boolean;
  color: string;
  name: string;
  id: string;
}

export interface EditorHistoryItem {
  route: MapReducer['route'];
}

export interface EditorState {
  changed: boolean;
  editing: boolean;
  ready: boolean;
  markers_shown: boolean;

  router: {
    points: LatLng[];
    waypoints: LatLng[];
  };

  mode: typeof modes[keyof typeof modes];

  dialog: typeof Dialogs[keyof typeof Dialogs];
  dialog_active: boolean;

  routerPoints: number;
  distance: number;
  estimated: number;
  speed: number;

  is_empty: boolean;
  is_published: boolean;
  is_routing: boolean;
  drawing_direction: typeof drawingDirections[keyof typeof drawingDirections];

  features: {
    routing: boolean;
  };

  renderer: {
    data: string;
    width: number;
    height: number;
    renderer_active: boolean;
    info: string;
    progress: number;
  };

  save: {
    error: string;
    finished: boolean;
    overwriting: boolean;
    processing: boolean;
    loading: boolean;
  };

  history: {
    records: EditorHistoryItem[];
    position: number;
  };

  gpx: {
    enabled: boolean;
    list: GpxRoute[];
  };
}

export const editorInitialState = {
  changed: false,
  editing: false,
  ready: false,
  markers_shown: false,

  mode: modes.NONE,
  dialog: null,
  dialog_active: false,

  routerPoints: 0,
  distance: 0,
  estimated: 0,
  speed: 15,
  drawing_direction: drawingDirections.FORWARDS,

  is_published: false,
  is_empty: true,
  is_routing: false,

  router: {
    waypoints: [],
    points: [],
  },

  features: {
    routing: false,
  },

  renderer: {
    data: '',
    width: 0,
    height: 0,
    renderer_active: false,
    info: '',
    progress: 0,
  },

  save: {
    error: null,
    finished: false,
    overwriting: false,
    processing: false,
    loading: false,
  },

  history: {
    records: [{ route: [] }],
    position: 0,
  },

  gpx: {
    enabled: true,
    list: [],
  },
};

export const editor = createReducer(editorInitialState, editorHandlers);
