import { ReduxState } from '../store';

export const selectEditor = (state: ReduxState) => state.editor;
export const selectEditorSave = (state: ReduxState) => state.editor.save;
export const selectEditorEditing = (state: ReduxState) => state.editor.editing;
export const selectEditorMode = (state: ReduxState) => state.editor.mode;
export const selectEditorRenderer = (state: ReduxState) => state.editor.renderer;
export const selectEditorRouter = (state: ReduxState) => state.editor.router;
export const selectEditorDistance = (state: ReduxState) => state.editor.distance;
export const selectEditorDirection = (state: ReduxState) => state.editor.drawing_direction;
export const selectEditorGpx = (state: ReduxState) => state.editor.gpx;
