import { Route } from '../map/types';
import { EditorState } from '.';
import { editorActions } from './constants';

export const setEditing = (editing: EditorState['editing']) => ({ type: editorActions.SET_EDITING, editing });

export const changeMode = (mode: EditorState['mode']) => ({ type: editorActions.CHANGE_MODE, mode });
export const setMode = (mode: EditorState['mode']) => ({ type: editorActions.SET_MODE, mode });

export const setDistance = (distance: EditorState['distance']) => ({ type: editorActions.SET_DISTANCE, distance });
export const setChanged = (changed: EditorState['changed']) => ({ type: editorActions.SET_CHANGED, changed });
export const setSpeed = (speed) => ({ type: editorActions.SET_SPEED, speed });

export const startEditing = () => ({ type: editorActions.START_EDITING });
export const stopEditing = () => ({ type: editorActions.STOP_EDITING });

export const routerCancel = () => ({ type: editorActions.ROUTER_CANCEL });
export const routerSubmit = () => ({ type: editorActions.ROUTER_SUBMIT });

export const clearAll = () => ({ type: editorActions.CLEAR_ALL });
export const clearCancel = () => ({ type: editorActions.CLEAR_CANCEL });

export const sendSaveRequest = (payload: {
  title: Route['title'];
  address: Route['address'];
  is_public: Route['is_public'];
  description: Route['description'];
  force: boolean;
}) => ({ type: editorActions.SEND_SAVE_REQUEST, ...payload });

export const resetSaveDialog = () => ({ type: editorActions.RESET_SAVE_DIALOG });
export const setSave = (save: Partial<EditorState['save']>) => ({ type: editorActions.SET_SAVE, save });
export const cancelSave = () => ({ type: editorActions.CANCEL_SAVE });

export const hideRenderer = () => ({ type: editorActions.HIDE_RENDERER });
export const setRenderer = (payload) => ({ type: editorActions.SET_RENDERER, payload });
export const takeAShot = () => ({ type: editorActions.TAKE_A_SHOT });
export const cropAShot = (payload: any) => ({ type: editorActions.CROP_A_SHOT, ...payload });
export const setDialog = (dialog: string) => ({ type: editorActions.SET_DIALOG, dialog });
export const setDialogActive = (dialog_active: boolean) => ({ type: editorActions.SET_DIALOG_ACTIVE, dialog_active });
export const setReady = (ready: boolean) => ({ type: editorActions.SET_READY, ready });

export const getGPXTrack = () => ({ type: editorActions.GET_GPX_TRACK });
export const setMarkersShown = (markers_shown) => ({ type: editorActions.SET_MARKERS_SHOWN, markers_shown });
export const setIsEmpty = (is_empty) => ({ type: editorActions.SET_IS_EMPTY, is_empty });
export const setFeature = (features: { [x: string]: boolean }) => ({ type: editorActions.SET_FEATURE, features });

export const setIsRouting = (is_routing: boolean) => ({ type: editorActions.SET_IS_ROUTING, is_routing });
export const setRouterPoints = (routerPoints: EditorState['routerPoints']) => ({
  type: editorActions.SET_ROUTER_POINTS,
  routerPoints,
});

export const locationChanged = (location) => ({ type: editorActions.LOCATION_CHANGED, location });

export const keyPressed = ({ key, target: { tagName } }: { key: string; target: { tagName: string } }) => ({
  type: editorActions.KEY_PRESSED,
  key,
  target: tagName,
});

export const setRouter = (router: Partial<EditorState['router']>) => ({ type: editorActions.SET_ROUTER, router });
export const setHistory = (history: Partial<EditorState['history']>) => ({ type: editorActions.SET_HISTORY, history });

export const undo = () => ({ type: editorActions.UNDO });
export const redo = () => ({ type: editorActions.REDO });

export const captureHistory = () => ({ type: editorActions.CAPTURE_HISTORY });
export const setDirection = (drawing_direction: EditorState['drawing_direction']) => ({
  type: editorActions.SET_DIRECTION,
  drawing_direction,
});

export const setGpx = (gpx: Partial<EditorState['gpx']>) => ({ type: editorActions.SET_GPX, gpx });
export const uploadGpx = (file: File) => ({ type: editorActions.UPLOAD_GPX, file });
export const dropGpx = (index: number) => ({ type: editorActions.DROP_GPX, index });
