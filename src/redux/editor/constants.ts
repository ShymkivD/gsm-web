const prefix = 'EDITOR';

export const gpxRouteColors = ['#ff3344', '#3344ff', '#33ff44', '#33ffff'];
export const drawingDirections = { FORWARDS: 'forward' as const, BACKWARDS: 'backward' as const };
export const editorHistoryLength = 100;

export const editorActions = {
  SET_EDITING: `${prefix}/SET_EDITING`,
  CHANGE_MODE: `${prefix}/CHANGE_MODE`,
  SET_MODE: `${prefix}/SET_MODE`,
  SET_DISTANCE: `${prefix}/SET_DISTANCE`,
  SET_CHANGED: `${prefix}/SET_CHANGED`,
  SET_SPEED: `${prefix}/SET_SPEED`,
  SET_ROUTER_POINTS: `${prefix}/SET_ROUTER_POINTS`,
  SET_ACTIVE_STICKER: `${prefix}/SET_ACTIVE_STICKER`,
  START_EDITING: `${prefix}/START_EDITING`,
  STOP_EDITING: `${prefix}/STOP_EDITING`,
  ROUTER_CANCEL: `${prefix}/ROUTER_CANCEL`,
  ROUTER_SUBMIT: `${prefix}/ROUTER_SUBMIT`,

  CLEAR_ALL: `${prefix}/CLEAR_ALL`,
  CLEAR_CANCEL: `${prefix}/CLEAR_CANCEL`,

  SEND_SAVE_REQUEST: `${prefix}/SEND_SAVE_REQUEST`,
  SET_SAVE_LOADING: `${prefix}/SET_SAVE_LOADING`,
  RESET_SAVE_DIALOG: `${prefix}/RESET_SAVE_DIALOG`,
  CANCEL_SAVE: `${prefix}/CANCEL_SAVE`,

  SET_SAVE: `${prefix}/SET_SAVE`,

  SHOW_RENDERER: `${prefix}/SHOW_RENDERER`,
  HIDE_RENDERER: `${prefix}/HIDE_RENDERER`,
  SET_RENDERER: `${prefix}/SET_RENDERER`,
  TAKE_A_SHOT: `${prefix}/TAKE_A_SHOT`,
  CROP_A_SHOT: `${prefix}/CROP_A_SHOT`,

  SET_DIALOG: `${prefix}/SET_DIALOG`,
  SET_DIALOG_ACTIVE: `${prefix}/SET_DIALOG_ACTIVE`,
  LOCATION_CHANGED: `${prefix}/LOCATION_CHANGED`,
  SET_READY: `${prefix}/SET_READY`,

  SET_MARKERS_SHOWN: `${prefix}/SET_MARKERS_SHOWN`,
  GET_GPX_TRACK: `${prefix}/GET_GPX_TRACK`,
  SET_IS_EMPTY: `${prefix}/SET_IS_EMPTY`,
  SET_FEATURE: `${prefix}/SET_FEATURE`,
  SET_IS_ROUTING: `${prefix}/SET_IS_ROUTING`,
  KEY_PRESSED: `${prefix}/KEY_PRESSED`,

  SET_ROUTER: `${prefix}/SET_ROUTER`,

  SET_HISTORY: `${prefix}/SET_HISTORY`,
  UNDO: `${prefix}/UNDO`,
  REDO: `${prefix}/REDO`,
  CAPTURE_HISTORY: `${prefix}/CAPTURE_HISTORY`,

  SET_DIRECTION: `${prefix}/SET_DIRECTION`,
  SET_GPX: `${prefix}/SET_GPX`,
  UPLOAD_GPX: `${prefix}/UPLOAD_GPX`,
  DROP_GPX: `${prefix}/DROP_GPX`,
};
