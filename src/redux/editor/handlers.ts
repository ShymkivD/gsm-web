import { getEstimated } from '../../utils/format';
import { EditorState } from '.';
import * as A from './actions';
import { editorActions } from './constants';

const setEditing = (state: EditorState, { editing }: ReturnType<typeof A.setEditing>) => ({
  ...state,
  editing,
});

const setChanged = (state: EditorState, { changed }: ReturnType<typeof A.setChanged>) => ({
  ...state,
  changed,
});

const setMode = (state: EditorState, { mode }: ReturnType<typeof A.changeMode>) => ({ ...state, mode });

const setDistance = (state: EditorState, { distance }: ReturnType<typeof A.setDistance>) => ({
  ...state,
  distance: parseFloat(distance.toFixed(1)),
  estimated: getEstimated(distance, state.speed),
});

const setRouterPoints = (state: EditorState, { routerPoints }: ReturnType<typeof A.setRouterPoints>) => ({
  ...state,
  routerPoints,
});

const hideRenderer = (state: EditorState) => ({ ...state, renderer: { ...state.renderer, renderer_active: false } });

const setRenderer = (state: EditorState, { payload }: ReturnType<typeof A.setRenderer>) => ({
  ...state,
  renderer: { ...state.renderer, ...payload },
});

const sendSaveRequest = (state: EditorState) => ({ ...state, save_processing: true });

const setSave = (state: EditorState, { save }: ReturnType<typeof A.setSave>) => ({
  ...state,
  save: { ...state.save, ...save },
});

const resetSaveDialog = (state: EditorState) => ({
  ...state,
  save_overwriting: false,
  save_finished: false,
  save_processing: false,
  save_error: '',
});

const setDialog = (state: EditorState, { dialog }: ReturnType<typeof A.setDialog>) => ({
  ...state,
  dialog,
});

const setDialogActive = (state: EditorState, { dialog_active }: ReturnType<typeof A.setDialogActive>) => ({
  ...state,
  dialog_active: dialog_active || !state.dialog_active,
});

const setReady = (state: EditorState, { ready = true }: ReturnType<typeof A.setReady>) => ({
  ...state,
  ready,
});

const setSpeed = (state: EditorState, { speed = 15 }: ReturnType<typeof A.setSpeed>) => ({
  ...state,
  speed,
  estimated: getEstimated(state.distance, speed),
});

const setMarkersShown = (
  state: EditorState,
  { markers_shown = true }: ReturnType<typeof A.setMarkersShown>,
): EditorState => ({ ...state, markers_shown });

const setIsEmpty = (state: EditorState, { is_empty = true }: ReturnType<typeof A.setIsEmpty>): EditorState => ({
  ...state,
  is_empty,
});

const setFeature = (state: EditorState, { features }: ReturnType<typeof A.setFeature>) => ({
  ...state,
  features: { ...state.features, ...features },
});

const setIsRouting = (state: EditorState, { is_routing }: ReturnType<typeof A.setIsRouting>) => ({
  ...state,
  is_routing,
});

const setRouter = (state: EditorState, { router }: ReturnType<typeof A.setRouter>) => ({
  ...state,
  router: { ...state.router, ...router },
});

const setHistory = (state: EditorState, { history }: ReturnType<typeof A.setHistory>) => ({
  ...state,
  history: { ...state.history, ...history },
});

const setDirection = (state: EditorState, { drawing_direction }: ReturnType<typeof A.setDirection>) => ({
  ...state,
  drawing_direction,
});

const setGpx = (state: EditorState, { gpx }: ReturnType<typeof A.setGpx>) => ({
  ...state,
  gpx: { ...state.gpx, ...gpx },
});

export const editorHandlers = {
  [editorActions.SET_EDITING]: setEditing,
  [editorActions.SET_CHANGED]: setChanged,
  [editorActions.SET_MODE]: setMode,
  [editorActions.SET_DISTANCE]: setDistance,
  [editorActions.SET_ROUTER_POINTS]: setRouterPoints,
  [editorActions.SET_SAVE]: setSave,
  [editorActions.SEND_SAVE_REQUEST]: sendSaveRequest,
  [editorActions.RESET_SAVE_DIALOG]: resetSaveDialog,
  [editorActions.HIDE_RENDERER]: hideRenderer,
  [editorActions.SET_RENDERER]: setRenderer,
  [editorActions.SET_DIALOG]: setDialog,
  [editorActions.SET_DIALOG_ACTIVE]: setDialogActive,
  [editorActions.SET_READY]: setReady,
  [editorActions.SET_SPEED]: setSpeed,
  [editorActions.SET_MARKERS_SHOWN]: setMarkersShown,
  [editorActions.SET_IS_EMPTY]: setIsEmpty,
  [editorActions.SET_FEATURE]: setFeature,
  [editorActions.SET_IS_ROUTING]: setIsRouting,
  [editorActions.SET_ROUTER]: setRouter,
  [editorActions.SET_HISTORY]: setHistory,
  [editorActions.SET_DIRECTION]: setDirection,
  [editorActions.SET_GPX]: setGpx,
};
