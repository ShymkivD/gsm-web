import { Tabs } from '../../constants/dialogs';
import { RootReducer, RootState } from '.';
import * as A from './actions';
import { userActions } from './constants';

type UnsafeReturnType<T> = T extends (...args: any[]) => infer R ? R : any;

export interface Handler<T> {
  (state: RootState, payload: UnsafeReturnType<T>): RootState;
}

const setUser: Handler<typeof A.setUser> = (state, { user }) => ({
  ...state,
  user: { ...state.user, ...user },
});

const searchSetTitle: Handler<typeof A.searchSetTitle> = (state, { title = '' }) => ({
  ...state,
  routes: { ...state.routes, filter: { ...state.routes.filter, title, distance: [0, 10000] } },
});

const searchSetDistance: Handler<typeof A.searchSetDistance> = (state, { distance = [0, 9999] }) => ({
  ...state,
  routes: {
    ...state.routes,
    filter: { ...state.routes.filter, distance: distance[1] >= 400 ? [distance[0], 9990] : distance },
  },
});

const searchSetTab: Handler<typeof A.searchSetTab> = (state, { tab = Tabs[Object.keys(Tabs)[0]] }) => ({
  ...state,
  routes: {
    ...state.routes,
    filter: { ...state.routes.filter, tab: Object.values(Tabs).indexOf(tab) >= 0 ? tab : Tabs[Object.values(Tabs)[0]] },
  },
});

const searchPutRoutes: Handler<typeof A.searchPutRoutes> = (state, { list = [], min, max, limit, step, shift }) => ({
  ...state,
  routes: {
    ...state.routes,
    list,
    limit,
    step,
    shift,
    filter: {
      ...state.routes.filter,
      distance: state.routes.filter.min === state.routes.filter.max ? [min, max] : state.routes.filter.distance,
      min,
      max,
    },
  },
});

const searchSetLoading: Handler<typeof A.searchSetLoading> = (state, { loading = false }) => ({
  ...state,
  routes: { ...state.routes, loading },
});

const setStarred: Handler<typeof A.setStarred> = (state, { is_published = false }) => ({
  ...state,
  is_published,
});

const mapsSetShift: Handler<typeof A.mapsSetShift> = (state, { shift = 0 }) => ({
  ...state,
  routes: { ...state.routes, shift },
});

const setRouteStarred: Handler<typeof A.setRouteStarred> = (state, { address, is_published }) => ({
  ...state,
  routes: {
    ...state.routes,
    list: state.routes.list
      .map((el) => (el.address === address ? { ...el, is_published } : el))
      .filter(
        (el) =>
          (state.routes.filter.tab === Tabs.STARRED && el.is_published) ||
          (state.routes.filter.tab === Tabs.PENDING && !el.is_published),
      ),
  },
});

const setLocation = (state: RootReducer, { location }: ReturnType<typeof A.setUserLocation>): RootReducer => ({
  ...state,
  location,
});

export const userHandlers = {
  [userActions.SET_USER]: setUser,
  [userActions.SET_USER_LOCATION]: setLocation,
  [userActions.SEARCH_SET_TITLE]: searchSetTitle,
  [userActions.SEARCH_SET_DISTANCE]: searchSetDistance,
  [userActions.SEARCH_CHANGE_DISTANCE]: searchSetDistance,
  [userActions.SEARCH_SET_TAB]: searchSetTab,
  [userActions.SEARCH_PUT_ROUTES]: searchPutRoutes,
  [userActions.SEARCH_SET_LOADING]: searchSetLoading,
  [userActions.MAPS_SET_SHIFT]: mapsSetShift,
  [userActions.SET_STARRED]: setStarred,
  [userActions.SET_ROUTE_STARRED]: setRouteStarred,
};
