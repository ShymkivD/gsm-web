import { User } from '../../constants/auth';
import { RootReducer } from '.';
import { userActions } from './constants';

export const setUser = (user: User) => ({ type: userActions.SET_USER, user });
export const userLogout = () => ({ type: userActions.USER_LOGOUT });
export const userLogin = () => ({ type: userActions.USER_LOGIN });

export const setStarred = (is_published: any) => ({ type: userActions.SET_STARRED, is_published });
export const openMapDialog = (tab) => ({ type: userActions.OPEN_MAP_DIALOG, tab });
export const gotOAuthUser = (user) => ({ type: userActions.GOT_OAUTH_USER, user });
export const searchSetTitle = (title) => ({ type: userActions.SEARCH_SET_TITLE, title });
export const searchSetDistance = (distance) => ({ type: userActions.SEARCH_SET_DISTANCE, distance });
export const searchChangeDistance = (distance) => ({ type: userActions.SEARCH_CHANGE_DISTANCE, distance });
export const searchSetTab = (tab) => ({ type: userActions.SEARCH_SET_TAB, tab });
export const searchSetLoading = (loading) => ({ type: userActions.SEARCH_SET_LOADING, loading });

export const searchPutRoutes = (payload) => ({ type: userActions.SEARCH_PUT_ROUTES, ...payload });
export const mapsLoadMore = () => ({ type: userActions.MAPS_LOAD_MORE });
export const mapsSetShift = (shift: number) => ({ type: userActions.MAPS_SET_SHIFT, shift });

export const dropRoute = (address: string) => ({ type: userActions.DROP_ROUTE, address });
export const modifyRoute = (address: string, { title, is_public }: { title: string; is_public: boolean }) => ({
  type: userActions.MODIFY_ROUTE,
  address,
  title,
  is_public,
});
export const toggleRouteStarred = (address: string) => ({ type: userActions.TOGGLE_ROUTE_STARRED, address });
export const setRouteStarred = (address: string, is_published: boolean) => ({
  type: userActions.SET_ROUTE_STARRED,
  address,
  is_published,
});

export const setUserLocation = (location: RootReducer['location']) => ({
  type: userActions.SET_USER_LOCATION,
  location,
});
