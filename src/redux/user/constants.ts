const prefix = 'USER';

export const userActions = {
  SET_USER: `${prefix}/SET_USER`,
  USER_LOGOUT: `${prefix}/USER_LOGOUT`,
  USER_LOGIN: `${prefix}/USER_LOGIN`,
  GOT_OAUTH_USER: `${prefix}/GOT_OAUTH_USER`,

  IFRAME_LOGIN: `${prefix}/IFRAME_LOGIN`,
  SET_USER_LOCATION: `${prefix}/SET_USER_LOCATION`,

  SEARCH_SET_TITLE: `${prefix}/SEARCH_SET_TITLE`,
  SEARCH_SET_DISTANCE: `${prefix}/SEARCH_SET_DISTANCE`,
  SEARCH_CHANGE_DISTANCE: `${prefix}/SEARCH_CHANGE_DISTANCE`,

  SEARCH_SET_TAB: `${prefix}/SEARCH_SET_TAB`,
  SEARCH_PUT_ROUTES: `${prefix}/SEARCH_PUT_ROUTES`,
  SEARCH_SET_LOADING: `${prefix}/SEARCH_SET_LOADING`,

  OPEN_MAP_DIALOG: `${prefix}/OPEN_MAP_DIALOG`,
  MAPS_LOAD_MORE: `${prefix}/MAPS_LOAD_MORE`,
  MAPS_SET_SHIFT: `${prefix}/MAPS_SET_SHIFT`,

  DROP_ROUTE: `${prefix}/DROP_ROUTE`,
  SET_STARRED: `${prefix}/SET_STARRED`,
  MODIFY_ROUTE: `${prefix}/MODIFY_ROUTE`,

  SET_ROUTE_STARRED: `${prefix}/SET_ROUTE_STARRED`,
  TOGGLE_ROUTE_STARRED: `${prefix}/TOGGLE_ROUTE_STARRED`,
};
