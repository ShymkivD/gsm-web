import { LatLngLiteral } from 'leaflet';

import { defaultUser, User } from '../../constants/auth';
import { createReducer } from '../../utils/reducer';
import { userHandlers } from './handlers';

export interface RouteListItem {
  address: string;
  title: string;
  distance: number;
  is_public: boolean;
  is_published: boolean;
  updated_at: string;
}

export interface RootReducer {
  user: User;
  location: LatLngLiteral;
  routes: {
    limit: 0;
    loading: boolean;
    list: Array<RouteListItem>;
    step: number;
    shift: number;
    filter: {
      title: string;
      starred: boolean;
      distance: [number, number];
      author: string;
      tab: string;
      min: number;
      max: number;
    };
  };
}

export type RootState = Readonly<RootReducer>;

export const userInitialState: RootReducer = {
  user: { ...defaultUser },
  location: null,
  routes: {
    limit: 0,
    loading: false, // <-- maybe delete this
    list: [],
    step: 20,
    shift: 0,
    filter: {
      title: '',
      starred: false,
      distance: [0, 10000],
      author: '',
      tab: '',
      min: 0,
      max: 10000,
    },
  },
};

export const userReducer = createReducer(userInitialState, userHandlers);
