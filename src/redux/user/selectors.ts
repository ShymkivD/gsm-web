import { ReduxState } from '../store';

export const selectUser = (state: ReduxState) => state.user;
export const selectUserUser = (state: ReduxState) => state.user.user;
export const selectUserLocation = (state: ReduxState) => state.user.location;
