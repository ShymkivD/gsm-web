import { REHYDRATE, RehydrateAction } from 'redux-persist';
import { call, delay, put, select, takeEvery, takeLatest } from 'redux-saga/effects';

import { defaultUser } from '../../constants/auth';
import { Dialogs, Tabs } from '../../constants/dialogs';
import {
  checkIframeToken,
  checkUserToken,
  dropRoute,
  getGuestToken,
  getRouteList,
  modifyRoute,
  sendRouteStarred,
} from '../../utils/api';
import { parseQuery, pushLoaderState, pushNetworkInitError } from '../../utils/history';
import { Unwrap } from '../../utils/middleware';
import { setDialog, setDialogActive } from '../editor/actions';
import { selectEditor } from '../editor/selectors';
import { mapInitSaga } from '../map/sagas';
import * as A from './actions';
import { userActions } from './constants';
import { selectUser, selectUserUser } from './selectors';

function* generateGuestSaga() {
  const {
    data: { user, random_url },
  }: Unwrap<typeof getGuestToken> = yield call(getGuestToken);

  yield put(A.setUser({ ...user, random_url }));
  return { ...user, random_url };
}

function* authCheckSaga({ key }: RehydrateAction) {
  if (key !== 'user') {
    return;
  }

  pushLoaderState(70);

  const { id, token }: ReturnType<typeof selectUserUser> = yield select(selectUserUser);
  const { ready }: ReturnType<typeof selectEditor> = yield select(selectEditor);
  console.log('auth saga', id, token, ready);

  if (window.location.search || true) {
    const { viewer_id, auth_key } = yield parseQuery(window.location.search);

    if (viewer_id && auth_key && id !== `google:${viewer_id}`) {
      const user = yield call(checkIframeToken, { viewer_id, auth_key });

      if (user) {
        yield put(A.setUser(user));
        pushLoaderState(99);

        return yield call(mapInitSaga);
      }
    }
  }

  if (id && token) {
    const {
      data: { user, random_url },
    }: Unwrap<typeof checkUserToken> = yield call(checkUserToken, { id, token });

    if (user) {
      yield put(A.setUser({ ...user, token, random_url }));
      pushLoaderState(99);

      return yield call(mapInitSaga);
    }

    if (!ready) {
      pushNetworkInitError();
      return;
    }
  }

  yield call(generateGuestSaga);
  pushLoaderState(80);

  return yield call(mapInitSaga);
}

function* gotOAuthUserSaga({ user: u }: ReturnType<typeof A.gotOAuthUser>) {
  const {
    data: { user, random_url },
  }: Unwrap<typeof checkUserToken> = yield call(checkUserToken, u);

  console.log('resp user', user);
  user.id = user.uid;
  user.token = u.token;
  yield put(A.setUser({ ...user, random_url }));
  yield put(A.userLogin());
}

function* searchGetRoutes() {
  const { token }: ReturnType<typeof selectUserUser> = yield select(selectUserUser);

  const {
    routes: {
      step,
      shift,
      filter: { title, distance, tab },
    },
  }: ReturnType<typeof selectUser> = yield select(selectUser);

  const result: Unwrap<typeof getRouteList> = yield getRouteList({
    token,
    search: title,
    min: distance[0],
    max: distance[1],
    step,
    shift,
    tab,
  });

  return result;
}

export function* searchSetSagaWorker() {
  const {
    routes: { filter },
  }: ReturnType<typeof selectUser> = yield select(selectUser);

  const {
    data: {
      routes,
      limits: { min, max, count: limit },
      filter: { shift, step },
    },
  }: Unwrap<typeof getRouteList> = yield call(searchGetRoutes);

  yield put(
    A.searchPutRoutes({
      list: routes,
      min,
      max,
      limit,
      shift,
      step,
    }),
  );

  // change distange range if needed and load additional data
  if (
    (filter.min > min && filter.distance[0] <= filter.min) ||
    (filter.max < max && filter.distance[1] >= filter.max)
  ) {
    yield put(
      A.searchChangeDistance([
        filter.min > min && filter.distance[0] <= filter.min ? min : filter.distance[0],
        filter.max < max && filter.distance[1] >= filter.max ? max : filter.distance[1],
      ]),
    );
  }

  return yield put(A.searchSetLoading(false));
}

function* searchSetSaga() {
  yield put(A.searchSetLoading(true));
  yield put(A.mapsSetShift(0));
  yield delay(300);
  yield call(searchSetSagaWorker);
}

function* openMapDialogSaga({ tab }: ReturnType<typeof A.openMapDialog>) {
  const {
    routes: {
      filter: { tab: current },
    },
  }: ReturnType<typeof selectUser> = yield select(selectUser);

  const { dialog_active }: ReturnType<typeof selectEditor> = yield select(selectEditor);

  if (dialog_active && tab === current) {
    return yield put(setDialogActive(false));
  }

  if (tab !== current) {
    yield put(A.searchSetTab(tab));
  }

  yield put(setDialog(Dialogs.MAP_LIST));
  yield put(setDialogActive(true));

  return tab;
}

function* searchSetTabSaga() {
  yield put(A.searchChangeDistance([0, 10000]));
  yield put(
    A.searchPutRoutes({
      list: [],
      min: 0,
      max: 10000,
      step: 20,
      shift: 0,
    }),
  );

  yield put(A.searchSetTitle(''));
}

function* userLogoutSaga() {
  yield put(A.setUser(defaultUser));
  yield call(generateGuestSaga);
}

function* setUserSaga() {
  const { dialog_active }: ReturnType<typeof selectEditor> = yield select(selectEditor);

  if (dialog_active) {
    yield call(searchSetSagaWorker);
  }

  return true;
}

function* mapsLoadMoreSaga() {
  const {
    routes: { limit, list, shift, step, loading, filter },
  }: ReturnType<typeof selectUser> = yield select(selectUser);

  if (loading || list.length >= limit || limit === 0) {
    return;
  }

  yield delay(50);

  yield put(A.searchSetLoading(true));
  yield put(A.mapsSetShift(shift + step));

  const {
    data: {
      limits: { min, max, count },
      filter: { shift: resp_shift, step: resp_step },
      routes,
    },
  }: Unwrap<typeof getRouteList> = yield call(searchGetRoutes);

  if (
    (filter.min > min && filter.distance[0] <= filter.min) ||
    (filter.max < max && filter.distance[1] >= filter.max)
  ) {
    yield put(
      A.searchChangeDistance([
        filter.min > min && filter.distance[0] <= filter.min ? min : filter.distance[0],
        filter.max < max && filter.distance[1] >= filter.max ? max : filter.distance[1],
      ]),
    );
  }

  yield put(
    A.searchPutRoutes({
      min,
      max,
      limit: count,
      shift: resp_shift,
      step: resp_step,
      list: [...list, ...routes],
    }),
  );
  yield put(A.searchSetLoading(false));
}

function* dropRouteSaga({ address }: ReturnType<typeof A.dropRoute>) {
  const { token }: ReturnType<typeof selectUserUser> = yield select(selectUserUser);
  const {
    routes: {
      list,
      step,
      shift,
      limit,
      filter: { min, max },
    },
  }: ReturnType<typeof selectUser> = yield select(selectUser);

  const index = list.findIndex((el) => el.address === address);

  if (index >= 0) {
    yield put(
      A.searchPutRoutes({
        list: list.filter((el) => el.address !== address),
        min,
        max,
        step,
        shift: shift > 0 ? shift - 1 : 0,
        limit: limit > 0 ? limit - 1 : limit,
      }),
    );
  }

  return yield call(dropRoute, { address, token });
}

function* modifyRouteSaga({ address, title, is_public }: ReturnType<typeof A.modifyRoute>) {
  const { token }: ReturnType<typeof selectUserUser> = yield select(selectUserUser);
  const {
    routes: {
      list,
      step,
      shift,
      limit,
      filter: { min, max },
    },
  }: ReturnType<typeof selectUser> = yield select(selectUser);

  const index = list.findIndex((el) => el.address === address);

  if (index >= 0) {
    yield put(
      A.searchPutRoutes({
        list: list.map((el) => (el.address !== address ? el : { ...el, title, is_public })),
        min,
        max,
        step,
        shift: shift > 0 ? shift - 1 : 0,
        limit: limit > 0 ? limit - 1 : limit,
      }),
    );
  }

  return yield call(modifyRoute, {
    address,
    token,
    title,
    is_public,
  });
}

function* toggleRouteStarredSaga({ address }: ReturnType<typeof A.toggleRouteStarred>) {
  const { token }: ReturnType<typeof selectUserUser> = yield select(selectUserUser);
  const {
    routes: { list },
  }: ReturnType<typeof selectUser> = yield select(selectUser);

  const route = list.find((el) => el.address === address);

  yield put(A.setRouteStarred(address, !route.is_published));
  const result = yield sendRouteStarred({
    token,
    address,
    is_published: !route.is_published,
  });

  if (!result) {
    return yield put(A.setRouteStarred(address, route.is_published));
  }
}

export function* updateUserRoutes() {
  yield put(A.searchSetTab(Tabs.MY));
}

export function* userSaga() {
  yield takeEvery(REHYDRATE, authCheckSaga);

  yield takeEvery(userActions.USER_LOGOUT, userLogoutSaga);
  yield takeLatest(userActions.GOT_OAUTH_USER, gotOAuthUserSaga);

  yield takeLatest([userActions.SEARCH_SET_TITLE, userActions.SEARCH_SET_DISTANCE], searchSetSaga);

  yield takeLatest(userActions.OPEN_MAP_DIALOG, openMapDialogSaga);
  yield takeLatest(userActions.SEARCH_SET_TAB, searchSetTabSaga);
  yield takeLatest(userActions.SET_USER, setUserSaga);

  yield takeLatest(userActions.MAPS_LOAD_MORE, mapsLoadMoreSaga);

  yield takeLatest(userActions.DROP_ROUTE, dropRouteSaga);
  yield takeLatest(userActions.MODIFY_ROUTE, modifyRouteSaga);
  yield takeLatest(userActions.TOGGLE_ROUTE_STARRED, toggleRouteStarredSaga);

  yield takeLatest([userActions.USER_LOGIN, userActions.USER_LOGOUT], updateUserRoutes);
}
