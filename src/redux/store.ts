import { createBrowserHistory } from 'history';
import { LatLngLiteral } from 'leaflet';
import { applyMiddleware, combineReducers, compose, createStore, Store } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import { Persistor } from 'redux-persist/es/types';
import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from 'redux-saga';

import { MainMap } from '../constants/map';
import { watchLocation } from '../utils/window';
import { editor, EditorState } from './editor';
import { locationChanged } from './editor/actions';
import { editorSaga } from './editor/sagas';
import { map, MapReducer } from './map';
import { mapZoomChange } from './map/actions';
import { mapSaga } from './map/sagas';
import { RootReducer, userReducer } from './user';
import { setUserLocation } from './user/actions';
import { userSaga } from './user/sagas';

// redux extension composer
const composeEnhancers =
  typeof window === 'object' && (<any>window).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? (<any>window).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

export interface ReduxState {
  user: RootReducer;
  map: MapReducer;
  editor: EditorState;
}
const userPersistConfig = { key: 'user', whitelist: ['user', 'provider', 'speed'], storage };
const editorPersistConfig = { key: 'editor', whitelist: ['gpx'], storage };

export const sagaMiddleware = createSagaMiddleware();

export const store = createStore(
  combineReducers({
    user: persistReducer(userPersistConfig, userReducer),
    editor: persistReducer(editorPersistConfig, editor),
    map,
  }),
  composeEnhancers(applyMiddleware(sagaMiddleware)),
);

export function configureStore(): { store: Store<any>; persistor: Persistor } {
  sagaMiddleware.run(userSaga);
  sagaMiddleware.run(mapSaga);
  sagaMiddleware.run(editorSaga);

  const persistor = persistStore(store);

  return { store, persistor };
}

export const history: any = createBrowserHistory();

history.listen((location, action) => {
  if (action === 'REPLACE') {
    return;
  }
  store.dispatch(locationChanged(location.pathname));
});

watchLocation((location: LatLngLiteral) => store.dispatch(setUserLocation(location)));
MainMap.on('zoomend', (event) => store.dispatch(mapZoomChange(event.target._zoom)));
