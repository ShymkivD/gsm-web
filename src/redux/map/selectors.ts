import { ReduxState } from '../store';

export const selectMap = (state: ReduxState) => state.map;
export const selectMapProvider = (state: ReduxState) => state.map.provider;
export const selectMapRoute = (state: ReduxState) => state.map.route;
export const selectMapTitle = (state: ReduxState) => state.map.title;
export const selectMapAddress = (state: ReduxState) => state.map.address;
export const selectMapZoom = (state: ReduxState) => state.map.zoom;
