import { LatLng } from 'leaflet';

import { defaultMapProvider } from '../../constants/providers';
import { createReducer } from '../../utils/reducer';
import { mapHandlers } from './handlers';

export interface MapReducer {
  provider: string;
  route: LatLng[];
  title: string;
  address: string;
  address_origin: string;
  description: string;
  owner: { id: string };
  is_public: boolean;
  zoom: number;
}

export const mapInitialState: MapReducer = {
  provider: defaultMapProvider,
  route: [],
  title: '',
  address: '',
  address_origin: '',
  description: '',
  owner: { id: null },
  is_public: false,
  zoom: 13,
};

export const map = createReducer(mapInitialState, mapHandlers);
