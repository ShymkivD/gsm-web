import { call, delay, put, race, select, take, TakeEffect, takeEvery, takeLatest } from 'redux-saga/effects';

import { MainMap } from '../../constants/map';
import { modes } from '../../constants/modes';
import { Tips } from '../../constants/tips';
import { getStoredMap, postMap } from '../../utils/api';
import { getUrlData, pushLoaderState, pushPath } from '../../utils/history';
import { Unwrap } from '../../utils/middleware';
import * as A from '../editor/actions';
import { editorActions } from '../editor/constants';
import { setReadySaga } from '../editor/sagas';
import { selectEditor } from '../editor/selectors';
import { selectUser, selectUserUser } from '../user/selectors';
import { mapSet, mapSetAddressOrigin, mapSetProvider, mapSetTitle } from './actions';
import { mapActions } from './constants';
import { selectMap, selectMapProvider, selectMapRoute } from './selectors';

export function* replaceAddressIfItsBusy(destination, original) {
  if (original) {
    yield put(mapSetAddressOrigin(original));
  }

  pushPath(`/${destination}/edit`);
}

export function* loadMapSaga(path: string) {
  const {
    data: { route },
  }: Unwrap<typeof getStoredMap> = yield call(getStoredMap, { address: path });

  if (route) {
    yield put(
      mapSet({
        provider: route.provider,
        route: route.route,
        title: route.title,
        address: route.address,
        description: route.description,
        is_public: route.is_public,
      }),
    );

    yield put(A.setHistory({ records: [{ route: route.route }] }));
    return { route };
  }

  return null;
}

export function* startEmptyEditorSaga() {
  yield put(A.setReady(false));

  const {
    user: { id, random_url },
  }: ReturnType<typeof selectUser> = yield select(selectUser);

  const { path, mode } = getUrlData();

  if (!path || !mode || mode !== 'edit') {
    pushPath(`/${random_url}/edit`);
  }

  yield put(A.clearAll());
  yield put(mapSet({ owner: { id } }));
  yield put(A.setEditing(true));
  yield put(A.setReady(true));
}

export function* loadMapFromPath() {
  const { path, mode } = getUrlData();
  yield put(A.setHistory({ records: [{ route: [] }], position: 0 }));

  if (path) {
    const map = yield call(loadMapSaga, path);

    if (!map) {
      yield call(startEmptyEditorSaga);
      return;
    }

    yield put(A.setEditing(!!(mode && mode === 'edit')));
    return;
  }

  yield call(startEmptyEditorSaga);
}

export function* mapInitSaga() {
  pushLoaderState(90);

  const { hash } = getUrlData();
  const provider: ReturnType<typeof selectMapProvider> = yield select(selectMapProvider);

  yield put(mapSetProvider(provider));

  if (hash && /^#map/.test(hash)) {
    const [, newUrl] = hash.match(/^#map[:/?!](.*)$/);

    if (newUrl) {
      yield pushPath(`/${newUrl}`);
      yield call(setReadySaga);
      return;
    }
  }

  yield call(loadMapFromPath);
  yield call(setReadySaga);
  MainMap.fitVisibleBounds({ animate: false });
  pushLoaderState(100);
}

function* setTitleSaga({ title }: ReturnType<typeof mapSetTitle>) {
  if (title) {
    document.title = `${title} | GSM Tracker`;
  }
}

function* startEditingSaga() {
  const { path } = getUrlData();
  yield pushPath(`/${path}/edit`);
}

function* clearAllSaga() {
  const route: ReturnType<typeof selectMapRoute> = yield select(selectMapRoute);

  if (!route.length) {
    return;
  }

  yield put(A.setChanged(false));
  yield put(mapSet({ route: [] }));
  yield put(A.captureHistory());
}

function* clearSaga({ type }) {
  switch (type) {
    case editorActions.CLEAR_ALL:
      yield call(clearAllSaga);
      break;

    default:
      break;
  }

  const { mode }: ReturnType<typeof selectEditor> = yield select(selectEditor);

  if (mode !== modes.NONE) {
    yield put(A.changeMode(modes.NONE));
  }
}

function* sendSaveRequestSaga({ title, address, is_public }: ReturnType<typeof A.sendSaveRequest>) {
  const { route, provider }: ReturnType<typeof selectMap> = yield select(selectMap);

  if (!route.length) {
    return yield put(
      A.setSave({
        error: Tips.SAVE_EMPTY,
        loading: false,
        overwriting: false,
        finished: false,
      }),
    );
  }

  const { distance }: ReturnType<typeof selectEditor> = yield select(selectEditor);
  const { token }: ReturnType<typeof selectUserUser> = yield select(selectUserUser);

  yield put(
    A.setSave({
      loading: true,
      overwriting: false,
      finished: false,
      error: null,
    }),
  );

  console.log({ token, route, title, address, distance, provider, is_public });
  const { result, timeout, cancel }: { result: Unwrap<typeof postMap>; timeout: boolean; cancel: TakeEffect } =
    yield race({
      result: postMap({ token, route, title, address, distance, provider, is_public }),
      timeout: delay(10000),
      cancel: take(editorActions.RESET_SAVE_DIALOG),
    });

  yield put(A.setSave({ loading: false }));

  if (cancel) {
    return yield put(A.changeMode(modes.NONE));
  }

  if (result && result.data.code === 'already_exist') {
    return yield put(A.setSave({ overwriting: true }));
  }

  if (result && result.data.code === 'conflict') {
    return yield put(
      A.setSave({
        error: Tips.SAVE_EXISTS,
        loading: false,
        overwriting: false,
        finished: false,
      }),
    );
  }

  if (timeout || !result || !result.data.route || !result.data.route.address) {
    return yield put(
      A.setSave({
        error: Tips.SAVE_TIMED_OUT,
        loading: false,
        overwriting: false,
        finished: false,
      }),
    );
  }

  yield put(
    mapSet({
      address: result.data.route.address,
      title: result.data.route.title,
      is_public: result.data.route.is_public,
      description: result.data.route.description,
    }),
  );

  yield put(A.setReady(false));
  pushPath(`/${address}/edit`);
  yield put(A.setReady(true));

  yield put(
    A.setSave({
      error: Tips.SAVE_SUCCESS,
      loading: false,
      overwriting: false,
      finished: true,
    }),
  );
}

function* setChanged() {
  const { changed } = yield select(selectEditor);
  if (changed) {
    return;
  }

  yield put(A.setChanged(true));
}

export function* mapSaga() {
  yield takeEvery([mapActions.SET_ROUTE], setChanged);

  yield takeEvery(editorActions.START_EDITING, startEditingSaga);
  yield takeEvery(mapActions.SET_TITLE, setTitleSaga);
  yield takeLatest(editorActions.SEND_SAVE_REQUEST, sendSaveRequestSaga);

  yield takeEvery([editorActions.CLEAR_ALL, editorActions.CLEAR_CANCEL], clearSaga);
}
