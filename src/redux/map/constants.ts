const prefix = 'MAP';

export const mapActions = {
  SET_MAP: `${prefix}/SET_MAP`,
  SET_PROVIDER: `${prefix}/SET_PROVIDER`,
  SET_ROUTE: `${prefix}/SET_ROUTE`,
  SET_TITLE: `${prefix}/SET_TILE`,
  SET_DESCRIPTION: `${prefix}/SETDESCRIPTION`,
  SET_ADDRESS: `${prefix}/SET_ADDRESS`,
  SET_ADDRESS_ORIGIN: `${prefix}/SET_ADDRESS_ORIGIN`,
  SET_OWNER: `${prefix}/SET_OWNER`,
  SET_PUBLIC: `${prefix}/SET_PUBLIC`,
  SET_LOGO: `${prefix}/SET_LOGO`,

  MAP_CLICKED: `${prefix}/MAP_CLICKED`,
  ZOOM_CHANGE: `${prefix}/ZOOM_CHANGE`,
};
