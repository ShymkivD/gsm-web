import { MapReducer } from '.';
import {
  mapSet,
  mapSetAddress,
  mapSetAddressOrigin,
  mapSetDescription,
  mapSetOwner,
  mapSetProvider,
  mapSetPublic,
  mapSetRoute,
  mapSetTitle,
  mapZoomChange,
} from './actions';
import { mapActions } from './constants';

const setMap = (state: MapReducer, { map }: ReturnType<typeof mapSet>) => ({ ...state, ...map });

const setProvider = (state: MapReducer, { provider }: ReturnType<typeof mapSetProvider>) => ({ ...state, provider });

const setRoute = (state: MapReducer, { route }: ReturnType<typeof mapSetRoute>) => ({ ...state, route });

const setTitle = (state: MapReducer, { title }: ReturnType<typeof mapSetTitle>) => ({ ...state, title });

const setAddress = (state: MapReducer, { address }: ReturnType<typeof mapSetAddress>) => ({ ...state, address });

const setDescription = (state: MapReducer, { description }: ReturnType<typeof mapSetDescription>) => ({
  ...state,
  description,
});

const setOwner = (state: MapReducer, { owner }: ReturnType<typeof mapSetOwner>) => ({ ...state, owner });

const setPublic = (state: MapReducer, { is_public }: ReturnType<typeof mapSetPublic>) => ({ ...state, is_public });

const setAddressOrigin = (state: MapReducer, { address_origin }: ReturnType<typeof mapSetAddressOrigin>) => ({
  ...state,
  address_origin,
});

const zoomChange = (state: MapReducer, { zoom }: ReturnType<typeof mapZoomChange>) => ({ ...state, zoom });

export const mapHandlers = {
  [mapActions.SET_MAP]: setMap,
  [mapActions.SET_PROVIDER]: setProvider,
  [mapActions.SET_ROUTE]: setRoute,
  [mapActions.SET_TITLE]: setTitle,
  [mapActions.SET_ADDRESS]: setAddress,
  [mapActions.SET_DESCRIPTION]: setDescription,
  [mapActions.SET_OWNER]: setOwner,
  [mapActions.SET_PUBLIC]: setPublic,
  [mapActions.SET_ADDRESS_ORIGIN]: setAddressOrigin,
  [mapActions.ZOOM_CHANGE]: zoomChange,
};
