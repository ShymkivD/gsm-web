import { LatLng } from 'leaflet';

import { MapReducer } from '.';
import { mapActions } from './constants';

export const mapSet = (map: Partial<MapReducer>) => ({ type: mapActions.SET_MAP, map });
export const mapSetProvider = (provider: MapReducer['provider']) => ({ type: mapActions.SET_PROVIDER, provider });
export const mapSetRoute = (route: MapReducer['route']) => ({ type: mapActions.SET_ROUTE, route });
export const mapClicked = (latlng: LatLng) => ({ type: mapActions.MAP_CLICKED, latlng });
export const mapSetTitle = (title: string) => ({ type: mapActions.SET_TITLE, title });
export const mapSetDescription = (description: string) => ({ type: mapActions.SET_DESCRIPTION, description });
export const mapSetAddress = (address: string) => ({ type: mapActions.SET_ADDRESS, address });
export const mapSetOwner = (owner: MapReducer['owner']) => ({ type: mapActions.SET_DESCRIPTION, owner });
export const mapSetPublic = (is_public: MapReducer['is_public']) => ({ type: mapActions.SET_PUBLIC, is_public });
export const mapZoomChange = (zoom: number) => ({ type: mapActions.ZOOM_CHANGE, zoom });
export const mapSetAddressOrigin = (address_origin: MapReducer['address_origin']) => ({
  type: mapActions.SET_ADDRESS_ORIGIN,
  address_origin,
});
