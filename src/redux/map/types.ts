import { LatLng } from 'leaflet';

export interface Route {
  version: number;
  title: string;
  owner: number;
  address: string;
  route: LatLng[];
  provider: string;
  is_public: boolean;
  is_published: boolean;
  description: string;
  distance: number;
}
