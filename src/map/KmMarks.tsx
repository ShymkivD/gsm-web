import pick from 'ramda/es/pick';
import { FC, memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';

import { MainMap } from '../constants/map';
import { selectMap } from '../redux/map/selectors';
import { ReduxState } from '../redux/store';
import { KmMarksLayer } from '../utils/marks';

const mapStateToProps = (state: ReduxState) => ({ map: pick(['route'], selectMap(state)) });
type Props = ReturnType<typeof mapStateToProps>;

const KmMarksUnconnected: FC<Props> = memo(({ map: { route } }) => {
  const [layer, setLayer] = useState(null);

  useEffect(() => {
    const layer = new KmMarksLayer([]);
    layer.addTo(MainMap);
    setLayer(layer);
    return () => MainMap.removeLayer(layer) as any;
  }, [MainMap]);

  useEffect(() => {
    if (!layer) {
      return () => null;
    }

    layer.setLatLngs(route);
  }, [layer, route]);
  return null;
});

export const KmMarks = connect(mapStateToProps)(KmMarksUnconnected);
