import { LatLng } from 'leaflet';
import { FC, memo, useCallback, useEffect, useState } from 'react';
import { connect } from 'react-redux';

import { MainMap } from '../constants/map';
import { modes } from '../constants/modes';
import { setDistance, setMarkersShown } from '../redux/editor/actions';
import { selectEditorDirection, selectEditorEditing, selectEditorMode } from '../redux/editor/selectors';
import { mapSetRoute } from '../redux/map/actions';
import { selectMapRoute } from '../redux/map/selectors';
import { ReduxState } from '../redux/store';
import { InteractivePoly } from '../utils/map/InteractivePoly';
import { isMobile } from '../utils/window';

const mapStateToProps = (state: ReduxState) => ({
  mode: selectEditorMode(state),
  editing: selectEditorEditing(state),
  route: selectMapRoute(state),
  drawing_direction: selectEditorDirection(state),
});
const mapDispatchToProps = { mapSetRoute, setDistance, setMarkersShown };
type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

const RouteUnconnected: FC<Props> = memo(
  ({ route, editing, mode, drawing_direction, mapSetRoute, setDistance, setMarkersShown }) => {
    const [layer, setLayer] = useState<InteractivePoly>(null);

    const onDistanceChange = useCallback(({ distance }) => setDistance(distance), [setDistance]);

    useEffect(() => {
      if (!MainMap) {
        return;
      }

      const interactive = new InteractivePoly([], {
        color: 'url(#activePathGradient)',
        weight: 6,
        maxMarkers: isMobile() ? 50 : 150,
        smoothFactor: 3,
      })
        .addTo(MainMap.routeLayer)
        .on('distancechange', onDistanceChange)
        .on('vertexdragstart', MainMap.disableClicks)
        .on('vertexdragend', MainMap.enableClicks)
        .on('vertexaddstart', MainMap.disableClicks)
        .on('vertexaddend', MainMap.enableClicks)
        .on('allvertexhide', () => setMarkersShown(false))
        .on('allvertexshow', () => setMarkersShown(true));

      setLayer(interactive);

      return () => {
        MainMap.routeLayer.removeLayer(interactive);
      };
    }, [MainMap, onDistanceChange]);

    const onRouteChanged = useCallback(
      ({ latlngs }) => {
        mapSetRoute(latlngs);
      },
      [mapSetRoute],
    );

    useEffect(() => {
      if (!layer) {
        return;
      }

      layer.on('latlngschange', onRouteChanged);

      return () => layer.off('latlngschange', onRouteChanged) as any;
    }, [layer, onRouteChanged]);

    useEffect(() => {
      if (!layer) {
        return;
      }

      const points = (route && route.length > 0 && route) || [];

      layer.setPoints(points as LatLng[]);
    }, [route, layer]);

    useEffect(() => {
      if (!layer) {
        return;
      }

      if (editing) {
        layer.editor.enable();
      } else {
        layer.editor.disable();
      }
    }, [editing, layer]);

    useEffect(() => {
      if (!layer) {
        return;
      }

      if (mode === modes.POLY && !layer.is_drawing) {
        layer.editor.continue();
      }

      if (mode !== modes.POLY && layer.is_drawing) {
        layer.editor.stop();
      }
    }, [mode, layer]);

    useEffect(() => {
      if (!layer) {
        return;
      }

      layer.setDirection(drawing_direction);
    }, [drawing_direction, layer]);

    return null;
  },
);

export const Route = connect(mapStateToProps, mapDispatchToProps)(RouteUnconnected);
