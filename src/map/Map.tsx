import 'leaflet/dist/leaflet.css';

import React, { memo } from 'react';
import { createPortal } from 'react-dom';
import { connect } from 'react-redux';

import { MainMap } from '../constants/map';
import { modes } from '../constants/modes';
import { selectEditorEditing, selectEditorGpx, selectEditorMode } from '../redux/editor/selectors';
import { mapClicked, mapSetRoute } from '../redux/map/actions';
import { selectMapProvider, selectMapRoute } from '../redux/map/selectors';
import { ReduxState } from '../redux/store';
import { selectUserLocation } from '../redux/user/selectors';
import { CurrentLocation } from './CurrentLocation';
import { GpxPolyline } from './GpxPolyline';
import { KmMarks } from './KmMarks';
import { Route } from './Route';
import { Router } from './Router';
import { TileLayer } from './TileLayer';

const mapStateToProps = (state: ReduxState) => ({
  provider: selectMapProvider(state),
  route: selectMapRoute(state),
  editing: selectEditorEditing(state),
  mode: selectEditorMode(state),
  location: selectUserLocation(state),
  gpx: selectEditorGpx(state),
});

const mapDispatchToProps = { mapSetRoute, mapClicked };

type Props = React.HTMLAttributes<HTMLDivElement> & ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

const MapUnconnected: React.FC<Props> = memo(({ provider, mode, location, gpx, mapClicked }) => {
  const onClick = React.useCallback(
    (event) => {
      if (!MainMap.clickable || mode === modes.NONE) {
        return;
      }

      mapClicked(event.latlng);
    },
    [mapClicked, mode],
  );

  React.useEffect(() => {
    MainMap.addEventListener('click', onClick);

    return () => {
      MainMap.removeEventListener('click', onClick);
    };
  }, [MainMap, onClick]);

  return createPortal(
    <div>
      <TileLayer provider={provider} map={MainMap} />

      <Route />
      <Router />
      <KmMarks />

      <CurrentLocation location={location} />
      {gpx.enabled &&
        gpx.list.map(
          ({ latlngs, enabled, color }, index) =>
            enabled && <GpxPolyline latlngs={latlngs} color={color} key={index} />,
        )}
    </div>,
    document.getElementById('canvas'),
  );
});

export const Map = connect(mapStateToProps, mapDispatchToProps)(MapUnconnected);
