import { FC, memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';

import { MainMap } from '../constants/map';
import { selectMapRoute } from '../redux/map/selectors';
import { ReduxState } from '../redux/store';
import { ArrowsLayer } from '../utils/map/ArrowsLayer';
// TODO: Unused component?
const mapStateToProps = (state: ReduxState) => ({ route: selectMapRoute(state) });
type Props = ReturnType<typeof mapStateToProps>;

const ArrowsUnconnected: FC<Props> = memo(({ route }) => {
  const [layer, setLayer] = useState(null);

  useEffect(() => {
    const item = new ArrowsLayer({}).addTo(MainMap);
    setLayer(item);
    return () => MainMap.removeLayer(item) as any;
  }, [MainMap]);

  useEffect(() => {
    if (!layer) {
      return;
    }

    layer.setLatLngs(route);
  }, [layer, route]);
  return null;
});

export const Arrows = connect(mapStateToProps)(ArrowsUnconnected);
