import { Map, TileLayer as TileLayerInterface, tileLayer } from 'leaflet';
import React from 'react';

import { defaultMapProvider, tileMaps } from '../constants/providers';
import { MapReducer } from '../redux/map';
import { TileContext } from '../utils/context';

type Props = React.HTMLAttributes<HTMLDivElement> & {
  provider: MapReducer['provider'];
  map: Map;
};

export const TileLayer: React.FC<Props> = React.memo(({ children, provider, map }) => {
  const [layer, setLayer] = React.useState<TileLayerInterface>(null);

  React.useEffect(() => {
    if (!map) {
      return;
    }

    setLayer(
      tileLayer(tileMaps[defaultMapProvider].url, {
        maxNativeZoom: 17,
        maxZoom: 17,
        minNativeZoom: 6.6,
        minZoom: 6.6,
      }).addTo(map),
    );
  }, [map]);

  React.useEffect(() => {
    if (!layer || !provider) {
      return;
    }

    const { url } = (provider && tileMaps[provider] && tileMaps[provider]) || tileMaps[defaultMapProvider];

    layer.setUrl(url);
  }, [layer, provider]);

  return <TileContext.Provider value={layer}>{children}</TileContext.Provider>;
});
