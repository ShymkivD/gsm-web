import { LatLngLiteral, Polyline } from 'leaflet';
import { FC, useEffect, useState } from 'react';

import { MainMap } from '../constants/map';

type Props = { latlngs: LatLngLiteral[]; color: string };

export const GpxPolyline: FC<Props> = ({ latlngs, color }) => {
  const [layer, setLayer] = useState<Polyline>(null);

  useEffect(() => {
    const item = new Polyline([], {
      color,
      stroke: true,
      opacity: 1,
      weight: 7,
    }).addTo(MainMap);
    setLayer(item);

    return () => MainMap.removeLayer(item) as any;
  }, [MainMap]);

  useEffect(() => {
    if (!layer) {
      return;
    }

    layer.setLatLngs(latlngs);
    layer.setStyle({ color });
  }, [latlngs, layer, color]);

  return null;
};
