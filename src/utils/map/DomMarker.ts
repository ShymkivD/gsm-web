import { DivIcon, DivIconOptions } from 'leaflet';

export interface DomMarkerProps extends DivIconOptions {
  element: HTMLElement;
}

export class DomMarker extends DivIcon {
  element: HTMLElement;

  constructor({ element, ...props }: DomMarkerProps) {
    super(props);
    this.element = element;
  }

  createIcon() {
    this.element.classList.add('icon');

    if (this.options.className) {
      this.element.classList.add(this.options.className);
    }
    return this.element;
  }
}
