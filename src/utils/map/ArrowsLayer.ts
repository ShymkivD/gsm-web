import { LatLng, LatLngLiteral, LayerGroup, Map, Marker, MarkerClusterGroup } from 'leaflet';

import { arrowClusterIcon, createArrow } from '../arrow';
import { angleBetweenPoints, dist2, middleCoord } from '../geom';

class ArrowsLayer extends LayerGroup {
  constructor(props) {
    super(props);
  }

  getChangeIndex = (prev: LatLngLiteral[], next: LatLngLiteral[]): number => {
    const changed_at = next.findIndex(
      (item, index) => !prev[index] || prev[index].lat != item.lat || prev[index].lng != item.lng,
    );

    return changed_at >= 0 ? changed_at : next.length;
  };

  setLatLngs = (route: LatLng[]): void => {
    if (!this.map) {
      return;
    }

    this.arrowLayer.clearLayers();

    if (route.length === 0) {
      return;
    }

    const midpoints = route.reduce(
      (res, latlng, i) =>
        route[i + 1] && dist2(route[i], route[i + 1]) > 0.0001
          ? [
              ...res,
              createArrow(
                middleCoord(route[i], route[i + 1]),
                angleBetweenPoints(
                  this.map.latLngToContainerPoint(route[i]),
                  this.map.latLngToContainerPoint(route[i + 1]),
                ),
              ),
            ]
          : res,
      [],
    );

    this.arrowLayer.addLayers(midpoints);
  };

  map: Map;

  arrowLayer = new MarkerClusterGroup({
    spiderfyOnMaxZoom: false,
    showCoverageOnHover: false,
    zoomToBoundsOnClick: false,
    animate: false,
    maxClusterRadius: 120, // 120
    iconCreateFunction: arrowClusterIcon,
  });

  layers: Marker<any>[] = [];
}

ArrowsLayer.addInitHook(function () {
  this.once('add', (event) => {
    if (event.target instanceof ArrowsLayer) {
      this.map = event.target._map;
      this.arrowLayer.addTo(this.map);
    }
  });

  this.once('remove', (event) => {
    if (event.target instanceof ArrowsLayer) {
      this.arrowLayer.removeFrom(this.map);
    }
  });
});

export { ArrowsLayer };
