import { Map, TileLayer } from 'leaflet';
import React from 'react';

export const MapContext = React.createContext<Map>(null);
export const TileContext = React.createContext<TileLayer>(null);
