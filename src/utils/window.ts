import { LatLngLiteral } from 'leaflet';

import { mobileBreakpoint } from '../config/frontend';

export const isMobile = (): boolean => window.innerWidth <= mobileBreakpoint;

export const getLocation = (callback: (pos: LatLngLiteral) => void) => {
  window.navigator.geolocation.getCurrentPosition((position) => {
    if (!position || !position.coords || !position.coords.latitude || !position.coords.longitude) {
      return callback(null);
    }

    const { latitude: lat, longitude: lng } = position.coords;

    callback({ lat, lng });
  });
};

export const watchLocation = (callback: (pos: LatLngLiteral) => void): number =>
  window.navigator.geolocation.watchPosition(
    (position) => {
      if (!position || !position.coords || !position.coords.latitude || !position.coords.longitude) {
        return callback(null);
      }

      const { latitude: lat, longitude: lng } = position.coords;

      callback({ lat, lng });
    },
    () => callback(null),
    {
      timeout: 30,
    },
  );
