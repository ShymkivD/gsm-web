import { AxiosRequestConfig } from 'axios';

export type Unwrap<T> = T extends (...args: any[]) => Promise<infer U> ? U : T;

export interface ApiErrorResult {
  detail?: string;
  code?: string;
}

export interface ResultWithStatus<T> {
  status: any;
  data?: Partial<T> & ApiErrorResult;
  error?: string;
  debug?: string;
}

export const httpResponses = {
  SUCCESS: 200,
  CREATED: 201,
  CONNECTION_REFUSED: 408,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  NOT_FOUND: 404,
  TOO_MANY_REQUESTS: 429,
};

export const resultMiddleware = ({ status, data }: { status: number; data: any }) => ({
  status,
  data,
});

export const errorMiddleware = <T extends any>(debug): ResultWithStatus<T> =>
  debug?.response
    ? debug.response
    : {
        status: httpResponses.CONNECTION_REFUSED,
        data: {},
        debug,
        error: 'Network error',
      };

export const configWithToken = (token: string, config: AxiosRequestConfig = {}): AxiosRequestConfig => ({
  ...config,
  headers: { ...(config.headers || {}), Authorization: `Bearer ${token}` },
});
