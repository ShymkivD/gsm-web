import axios from 'axios/index';
import { LatLngLiteral } from 'leaflet';

import { client } from '../config/frontend';
import { api } from '../constants/api';
import { User } from '../constants/auth';
import { Route } from '../redux/map/types';
import { RootState, RouteListItem } from '../redux/user';
import { configWithToken, errorMiddleware, resultMiddleware, ResultWithStatus } from './middleware';

interface GetRouteList {
  min: number;
  max: number;
  tab: string;
  search: string;
  step: RootState['routes']['step'];
  shift: RootState['routes']['step'];
  token: RootState['user']['token'];
}

export const checkUserToken = ({
  id,
  token,
}: {
  id: string;
  token: string;
}): Promise<ResultWithStatus<{ user: User; random_url: string; routes: RouteListItem[] }>> =>
  axios.get(api.checkToken, { params: { id, token } }).then(resultMiddleware).catch(errorMiddleware);

export const getGuestToken = (): Promise<ResultWithStatus<{ user: User; random_url: string }>> =>
  axios.get(api.getGuest).then(resultMiddleware).catch(errorMiddleware);

export const getStoredMap = ({
  address,
}: {
  address: Route['address'];
}): Promise<
  ResultWithStatus<{
    route: Route;
    error?: string;
    random_url: string;
  }>
> => axios.get(api.getMap, { params: { address } }).then(resultMiddleware).catch(errorMiddleware);

export const postMap = ({
  title,
  address,
  route,
  distance,
  provider,
  is_public,
  token,
}: Partial<Route> & { token: string }): Promise<ResultWithStatus<{ route: Route; error?: string; code?: string }>> =>
  axios
    .put(api.postMap, { title, address, route, distance, provider, is_public }, configWithToken(token))
    .then(resultMiddleware)
    .catch(errorMiddleware);

export const checkIframeToken = ({ viewer_id, auth_key }: { viewer_id: string; auth_key: string }) =>
  axios
    .get(api.iframeLogin, {
      params: { viewer_id, auth_key },
    })
    .then((result) => {
      console.log('auth endpoiint');
      console.log(result);
      return result?.data?.success && result?.data?.user;
    })
    .catch(() => false);

export const getRouteList = ({
  search,
  min,
  max,
  tab,
  token,
  step,
  shift,
}: GetRouteList): Promise<
  ResultWithStatus<{
    routes: Route[];
    limits: { min: number; max: number; count: number };
    filter: { min: number; max: number; shift: number; step: number };
  }>
> =>
  axios
    .get(api.getRouteList(tab), configWithToken(token, { params: { search, min, max, token, step, shift } }))
    .then(resultMiddleware)
    .catch(errorMiddleware);

export const checkOSRMService = (bounds: LatLngLiteral[]): Promise<boolean> =>
  client &&
  client.osrmUrl &&
  axios
    .get(client.osrmTestUrl(bounds))
    .then(() => true)
    .catch(() => false);

export const dropRoute = ({ address, token }: { address: string; token: string }): Promise<any> =>
  axios
    .delete(api.dropRoute, configWithToken(token, { data: { address } }))
    .then(resultMiddleware)
    .catch(errorMiddleware);

export const modifyRoute = ({
  address,
  token,
  title,
  is_public,
}: {
  address: string;
  token: string;
  title: string;
  is_public: boolean;
}): Promise<ResultWithStatus<{ route: Route }>> =>
  axios
    .patch(api.modifyRoute, { address, is_public, title }, configWithToken(token))
    .then(resultMiddleware)
    .catch(errorMiddleware);

export const sendRouteStarred = ({
  token,
  address,
  is_published,
}: {
  token: string;
  address: string;
  is_published: boolean;
}): Promise<ResultWithStatus<{ route: Route }>> =>
  axios
    .post(api.setStarred, { address, is_published }, configWithToken(token))
    .then(resultMiddleware)
    .catch(errorMiddleware);
