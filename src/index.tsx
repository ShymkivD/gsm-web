import './styles/_main.scss';

import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import { App } from './containers/App';
import { configureStore } from './redux/store';
import { pushLoaderState } from './utils/history';

const { store, persistor } = configureStore();

pushLoaderState(10);

const Index = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>
);

render(<Index />, document.getElementById('index'));
