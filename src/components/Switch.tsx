import classnames from 'classnames';

type Props = {
  active: boolean;
  onPress?: () => void;
};

export const Switch = ({ active, onPress = () => null }: Props) => (
  <div className={classnames('switch', { active })} onMouseDown={onPress} />
);
