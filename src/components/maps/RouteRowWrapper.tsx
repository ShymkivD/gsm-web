import classnames from 'classnames';
import { FC, memo } from 'react';

import { RouteRowDrop, RouteRowEditor, RouteRowView } from '.';

interface Props {
  address: string;
  tab: string;
  role: string;
  title: string;
  distance: number;
  is_public: boolean;
  is_published: boolean;

  is_admin: boolean;
  is_editing_target: boolean;
  is_menu_target: boolean;

  openRoute: any;
  startEditing: any;
  stopEditing: any;
  showMenu: any;
  hideMenu: any;
  showDropCard: any;
  dropRoute: any;
  modifyRoute: any;
  toggleStarred: any;

  is_editing_mode: 'edit' | 'drop';
}

export const RouteRowWrapper: FC<Props> = memo(
  ({
    title,
    distance,
    address,
    openRoute,
    tab,
    startEditing,
    showMenu,
    showDropCard,
    is_public,
    is_editing_target,
    is_menu_target,
    is_editing_mode,
    dropRoute,
    stopEditing,
    modifyRoute,
    hideMenu,
    is_admin,
    is_published,
    toggleStarred,
    role,
  }) => (
    <div
      className={classnames('route-row-wrapper', {
        is_menu_target,
        is_editing_target,
      })}
    >
      {is_editing_target && is_editing_mode === 'edit' && (
        <RouteRowEditor title={title} address={address} is_public={is_public} modifyRoute={modifyRoute} />
      )}
      {is_editing_target && is_editing_mode === 'drop' && (
        <RouteRowDrop address={address} dropRoute={dropRoute} stopEditing={stopEditing} />
      )}
      {!is_editing_target && (
        <RouteRowView
          address={address}
          tab={tab}
          title={title}
          distance={distance}
          is_public={is_public}
          is_published={is_published}
          openRoute={openRoute}
          startEditing={startEditing}
          stopEditing={stopEditing}
          showMenu={showMenu}
          hideMenu={hideMenu}
          showDropCard={showDropCard}
          is_admin={is_admin}
          toggleStarred={toggleStarred}
          role={role}
        />
      )}
    </div>
  ),
);
