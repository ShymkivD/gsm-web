import { FC, memo } from 'react';

import { dropRoute } from '../../redux/user/actions';

type Props = {
  address: string;
  dropRoute: typeof dropRoute;
  stopEditing: any;
};

export const RouteRowDrop: FC<Props> = memo(({ address, stopEditing, dropRoute }) => (
  <div className="route-row-drop">
    <div className="route-row">
      <div className="button-group">
        <div className="button" onClick={dropRoute.bind(null, address)}>
          Delete
        </div>
        <div className="button primary" onClick={stopEditing}>
          Cancel
        </div>
      </div>
    </div>
  </div>
));
