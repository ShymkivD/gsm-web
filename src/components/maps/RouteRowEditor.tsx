import { Component } from 'react';

import { Switch } from '../Switch';

interface Props {
  title: string;
  address: string;
  is_public: boolean;
  modifyRoute: any;
}

interface State {
  title: string;
  is_public: boolean;
}

export class RouteRowEditor extends Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      title: props.title,
      is_public: props.is_public,
    };
  }

  stopEditing = () => {
    const {
      state: { title, is_public },
      props: { address },
    } = this;

    this.props.modifyRoute({ address, title, is_public });
  };

  setPublic = () => this.setState({ is_public: !this.state.is_public });

  setTitle = ({ target: { value } }: { target: { value: string } }) => this.setState({ title: value });

  render() {
    const {
      state: { title, is_public },
    } = this;

    return (
      <div className="route-row-edit">
        <div className="route-row">
          <div className="route-title">
            <input type="text" value={title} onChange={this.setTitle} placeholder="Enter name" autoFocus />
          </div>
          <div className="route-row-editor">
            <div className="route-row-buttons">
              <div className="flex_1" onClick={this.setPublic}>
                <Switch active={is_public} />
                {is_public ? ' In the map catalog' : ' Link only'}
              </div>
              <div className="button primary" onClick={this.stopEditing}>
                OK
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
