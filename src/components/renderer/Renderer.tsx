import 'croppr/dist/croppr.css';

import Croppr from 'croppr';
import React from 'react';
import { connect } from 'react-redux';

import { cropAShot, hideRenderer } from '../../redux/editor/actions';
import { selectEditor } from '../../redux/editor/selectors';
import { selectMap } from '../../redux/map/selectors';
import { ReduxState } from '../../redux/store';
import { RendererPanel } from '../panels/RendererPanel';

const mapStateToProps = (state: ReduxState) => ({ editor: selectEditor(state), map: selectMap(state) });
const mapDispatchToProps = { hideRenderer, cropAShot };
type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps & { onClick: () => void };
type State = { opacity: number };

class Component extends React.Component<Props, State> {
  state = { opacity: 0 };

  onImageLoaded = () => {
    this.croppr = new Croppr(this.image, {});
    this.setState({ opacity: 1 });
  };

  componentWillUnmount() {
    if (this.croppr) {
      this.croppr.destroy();
    }
  }

  croppr: Croppr;
  image: HTMLImageElement;
  getImage = () => this.props.cropAShot(this.croppr.getValue());

  render() {
    const { data } = this.props.editor.renderer;
    const { opacity } = this.state;
    const { innerWidth, innerHeight } = window;
    const padding = 30;
    const paddingBottom = 80;

    const height = innerHeight - padding - paddingBottom;
    const width = height * (innerWidth / innerHeight);

    return (
      <div>
        <div className="renderer-shade" style={{ padding, paddingBottom }}>
          <div style={{ width, height, opacity }}>
            <img
              src={data}
              alt=""
              id="rendererOutput"
              ref={(el) => {
                this.image = el;
              }}
              onLoad={this.onImageLoaded}
            />
          </div>
        </div>

        <RendererPanel onCancel={this.props.hideRenderer} onSubmit={this.getImage} />
      </div>
    );
  }
}

export const Renderer = connect(mapStateToProps, mapDispatchToProps)(Component);
