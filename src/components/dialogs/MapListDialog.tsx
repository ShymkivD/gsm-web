import classnames from 'classnames';
import { PureComponent } from 'react';
import { connect } from 'react-redux';

import { Role } from '../../constants/auth';
import { Tabs } from '../../constants/dialogs';
import { setDialogActive } from '../../redux/editor/actions';
import { ReduxState } from '../../redux/store';
import { RouteListItem } from '../../redux/user';
import {
  dropRoute,
  mapsLoadMore,
  modifyRoute,
  searchSetDistance,
  searchSetTab,
  searchSetTitle,
  toggleRouteStarred,
} from '../../redux/user/actions';
import { pushPath } from '../../utils/history';
import { isMobile } from '../../utils/window';
import { RouteRowWrapper } from '../maps';
import { Icon } from '../panels';
import { Scroll } from '../Scroll';
import { MapListDialogHead } from '../search/MapListDialogHead';
import { DialogLoader } from './DialogLoader';

const mapStateToProps = ({
  editor: { editing },
  user: {
    routes,
    user: { role },
  },
}: ReduxState) => ({ role, routes, editing, ready: routes.filter.max < 9999 });

const mapDispatchToProps = {
  searchSetDistance,
  searchSetTitle,
  searchSetTab,
  setDialogActive,
  mapsLoadMore,
  dropRoute,
  modifyRoute,
  toggleRouteStarred,
};

type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

export interface State {
  menu_target: RouteListItem['address'];
  editor_target: RouteListItem['address'];

  is_editing: boolean;
  is_dropping: boolean;
}

class MapListDialogUnconnected extends PureComponent<Props, State> {
  state = { menu_target: null, editor_target: null, is_editing: false, is_dropping: false };

  startEditing = (editor_target: RouteListItem['address']): void =>
    this.setState({ editor_target, menu_target: null, is_editing: true, is_dropping: false });
  showMenu = (menu_target: RouteListItem['address']): void => this.setState({ menu_target });
  hideMenu = (): void => this.setState({ menu_target: null });
  showDropCard = (editor_target: RouteListItem['address']): void =>
    this.setState({ editor_target, menu_target: null, is_editing: false, is_dropping: true });
  stopEditing = (): void => this.setState({ editor_target: null });
  setTitle = ({ target: { value } }: { target: { value: string } }): void => {
    this.props.searchSetTitle(value);
  };

  openRoute = (_id: string): void => {
    if (isMobile()) {
      this.props.setDialogActive(false);
    }

    this.stopEditing();
    pushPath(`/${_id}`);
  };

  onScroll = (e: { target: { scrollHeight: number; scrollTop: number; clientHeight: number } }): void => {
    const {
      target: { scrollHeight, scrollTop, clientHeight },
    } = e;
    const delta = scrollHeight - scrollTop - clientHeight;

    if (delta < 500 && this.props.routes.list.length < this.props.routes.limit && !this.props.routes.loading) {
      this.props.mapsLoadMore();
    }
  };

  modifyRoute = ({ address, title, is_public }: { address: string; title: string; is_public: boolean }): void => {
    this.props.modifyRoute(address, { title, is_public });
    this.stopEditing();
  };

  toggleStarred = (id: string) => this.props.toggleRouteStarred(id);

  render() {
    const {
      ready,
      role,
      routes: {
        list,
        loading,
        filter: { min, max, title, distance },
      },
    }: Props = this.props;

    const { editor_target, menu_target, is_dropping } = this.state;

    return (
      <div className="dialog-content full">
        {list.length === 0 && loading && <DialogLoader />}

        {ready && !loading && list.length === 0 && (
          <div className="dialog-maplist-loader">
            <div className="dialog-maplist-icon">
              <Icon icon="icon-sad-1" />
            </div>
            HERE'S EMPTY <br />
            AND LONELY
          </div>
        )}

        <MapListDialogHead
          min={min}
          max={max}
          distance={distance}
          onDistanceChange={this.props.searchSetDistance}
          ready={ready}
          search={title}
          onSearchChange={this.setTitle}
        />

        <Scroll className="dialog-shader" onScroll={this.onScroll}>
          <div className="dialog-maplist">
            {list.map((route) => (
              <RouteRowWrapper
                title={route.title}
                distance={route.distance}
                address={route.address}
                is_public={route.is_public}
                is_published={route.is_published}
                tab={role === Role.guest ? Tabs.STARRED : Tabs.MY}
                is_editing_mode={is_dropping ? 'drop' : 'edit'}
                is_editing_target={editor_target === route.address}
                is_menu_target={menu_target === route.address}
                openRoute={this.openRoute}
                startEditing={this.startEditing}
                stopEditing={this.stopEditing}
                showMenu={this.showMenu}
                hideMenu={this.hideMenu}
                showDropCard={this.showDropCard}
                dropRoute={this.props.dropRoute}
                modifyRoute={this.modifyRoute}
                toggleStarred={this.toggleStarred}
                key={route.address}
                is_admin={role === Role.admin}
                role={role}
              />
            ))}
          </div>
        </Scroll>

        <div className={classnames('dialog-maplist-pulse', { active: loading })} />
      </div>
    );
  }
}

export const MapListDialog = connect(mapStateToProps, mapDispatchToProps)(MapListDialogUnconnected);
