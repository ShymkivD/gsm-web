import { FC } from 'react';
import { connect } from 'react-redux';

import { clearAll, clearCancel } from '../../redux/editor/actions';

const mapStateToProps = () => ({});
const mapDispatchToProps = { clearAll, clearCancel };
type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps & { width: number };

const TrashDialogUnconnected: FC<Props> = ({ clearAll, clearCancel, width }) => (
  <div className="control-dialog control-dialog__medium" style={{ width }}>
    <div className="helper trash-helper desktop-only">
      <div className="helper__text danger">
        <div className="big upper desktop-only">All changes will be deleted!</div>
      </div>
    </div>
    <div className="helper trash-helper">
      <div className="helper__buttons flex_1 trash-buttons">
        <div className="flex_1" />
        <div className="button-group">
          <div className="button router-helper__button" onClick={clearAll}>
            Clear
          </div>
        </div>

        <div className="button primary router-helper__button" onClick={clearCancel}>
          Cancel
        </div>
      </div>
    </div>
  </div>
);

export const TrashDialog = connect(mapStateToProps, mapDispatchToProps)(TrashDialogUnconnected);
