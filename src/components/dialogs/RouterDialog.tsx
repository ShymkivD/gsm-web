import classnames from 'classnames';
import { FC } from 'react';
import { connect } from 'react-redux';

import { routerCancel, routerSubmit } from '../../redux/editor/actions';
import { selectEditorRouter } from '../../redux/editor/selectors';
import { ReduxState } from '../../redux/store';
import { Icon } from '../panels';

const noPoints = ({ routerCancel }) => (
  <>
    <div className="helper router-helper">
      <div className="helper__text">
        <Icon icon="icon-pin-1" />
        <div className="big white upper">Put the first point on the map</div>
      </div>
    </div>

    <div className="helper router-helper">
      <div className="flex_1" />

      <div className="helper__buttons">
        <div className="button router-helper__button" onClick={routerCancel}>
          Cancel
        </div>
      </div>
    </div>
  </>
);

const firstPoint = ({ routerCancel }) => (
  <>
    <div className="helper router-helper">
      <div className="helper__text">
        <Icon icon="icon-pin-1" />
        <div className="big white upper">SET FOLLOWING POINT</div>
      </div>
    </div>
    <div className="helper router-helper">
      <div className="flex_1" />

      <div className="helper__buttons">
        <div className="button router-helper__button" onClick={routerCancel}>
          Cancel
        </div>
      </div>
    </div>
  </>
);

const draggablePoints = ({ routerCancel, routerSubmit }) => (
  <>
    <div className="helper">
      <div className="helper__text success">
        <Icon icon="icon-check-1" />
        <div className="big upper">Continue the route</div>
      </div>
    </div>
    <div className="helper router-helper">
      <div className="flex_1" />

      <div className="helper__buttons button-group">
        <div className="button button_red router-helper__button" onClick={routerCancel}>
          Cancel
        </div>
        <div className="button primary router-helper__button" onClick={routerSubmit}>
          Apply
        </div>
      </div>
    </div>
  </>
);

const mapStateToProps = (state: ReduxState) => ({ router: selectEditorRouter(state) });
const mapDispatchToProps = { routerCancel, routerSubmit };
type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

const RouterDialogUnconnected: FC<Props> = ({ router: { waypoints }, routerCancel, routerSubmit }) => (
  <div className="control-dialog control-dialog__medium">
    <div className={classnames('save-loader')} />

    {!waypoints.length && noPoints({ routerCancel })}
    {waypoints.length === 1 && firstPoint({ routerCancel })}
    {waypoints.length >= 2 && draggablePoints({ routerCancel, routerSubmit })}
  </div>
);

export const RouterDialog = connect(mapStateToProps, mapDispatchToProps)(RouterDialogUnconnected);
