import classnames from 'classnames';
import { connect } from 'react-redux';

import { replaceProviderUrl, tileMaps } from '../../constants/providers';
import { mapSetProvider } from '../../redux/map/actions';
import { selectMapProvider } from '../../redux/map/selectors';
import { ReduxState } from '../../redux/store';
import { Icon } from '../panels';

const mapStateToProps = (state: ReduxState) => ({ provider: selectMapProvider(state) });
const mapDispatchToProps = { mapSetProvider };
type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

const ProviderDialogUnconnected = ({ provider, mapSetProvider }: Props) => (
  <div className="control-dialog top right control-dialog-provider">
    <div className="helper provider-helper">
      {Object.keys(tileMaps).map((item) => (
        <div
          className={classnames('provider-helper-thumb', { active: provider === item })}
          style={{
            backgroundImage: `url(${replaceProviderUrl(item, { x: 19810, y: 10246, zoom: 15 })})`,
          }}
          onMouseDown={() => mapSetProvider(item)}
          key={tileMaps[item].name}
        >
          {provider === item && (
            <div className="provider-helper-check">
              <Icon icon="icon-check-1" />
            </div>
          )}
        </div>
      ))}
    </div>
  </div>
);

export const ProviderDialog = connect(mapStateToProps, mapDispatchToProps)(ProviderDialogUnconnected);
