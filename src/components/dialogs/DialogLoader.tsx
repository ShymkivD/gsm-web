import { FC } from 'react';

import { Icon } from '../panels';

export const DialogLoader: FC = ({}) => (
  <div className="dialog-maplist-loader">
    <div className="dialog-maplist-icon spin">
      <Icon icon="icon-sync-1" />
    </div>
  </div>
);
