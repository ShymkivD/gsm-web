import classNames from 'classnames';
import { latLngBounds } from 'leaflet';
import { ChangeEvent, FC, useCallback } from 'react';
import { connect } from 'react-redux';
import { v4 as uuid } from 'uuid';

import { MainMap } from '../../constants/map';
import { dropGpx, getGPXTrack, setGpx, uploadGpx } from '../../redux/editor/actions';
import { selectEditorGpx } from '../../redux/editor/selectors';
import { mapSetRoute } from '../../redux/map/actions';
import { selectMapAddress, selectMapRoute, selectMapTitle } from '../../redux/map/selectors';
import { ReduxState } from '../../redux/store';
import { getRandomColor } from '../../utils/dom';
import { getUrlData } from '../../utils/history';
import { simplify } from '../../utils/simplify';
import { GpxDialogRow } from '../gpx';
import { Switch } from '../Switch';

const mapStateToProps = (state: ReduxState) => ({
  gpx: selectEditorGpx(state),
  route: selectMapRoute(state),
  title: selectMapTitle(state),
  address: selectMapAddress(state),
});
const mapDispatchToProps = { dropGpx, uploadGpx, setGpx, getGPXTrack, mapSetRoute };
type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

const GpxDialogUnconnected: FC<Props> = ({
  title,
  address,
  gpx,
  route,
  getGPXTrack,
  setGpx,
  uploadGpx,
  mapSetRoute,
}) => {
  const toggleGpx = useCallback(() => {
    setGpx({ enabled: !gpx.enabled });
  }, [gpx, setGpx]);

  const onGpxUpload = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      if (!event.target || !event.target.files || event.target.files.length === 0) {
        return;
      }

      uploadGpx(event.target.files[0]);
    },
    [uploadGpx],
  );

  const onFocusRoute = useCallback(
    (index) => {
      if (!gpx.list[index] || !gpx.list[index].latlngs) {
        return;
      }

      const bounds = latLngBounds(gpx.list[index].latlngs);
      MainMap.fitBounds(bounds);
    },
    [gpx, MainMap],
  );

  const onRouteDrop = useCallback(
    (index) => {
      setGpx({ list: gpx.list.filter((el, i) => i !== index) });
    },
    [gpx, setGpx],
  );

  const onRouteColor = useCallback(
    (index) => {
      if (!gpx.enabled) {
        return;
      }
      setGpx({ list: gpx.list.map((el, i) => (i !== index ? el : { ...el, color: getRandomColor() })) });
    },
    [gpx, setGpx],
  );

  const onRouteToggle = useCallback(
    (index) => {
      if (!gpx.enabled) {
        return;
      }

      setGpx({
        list: gpx.list.map((el, i) => (i !== index ? el : { ...el, enabled: !el.enabled })),
      });
    },
    [gpx, setGpx],
  );

  const addCurrent = useCallback(() => {
    if (!route.length) {
      return;
    }

    const { path } = getUrlData();

    setGpx({
      list: [
        ...gpx.list,
        { latlngs: route, enabled: false, name: title || address || path, id: uuid(), color: getRandomColor() },
      ],
    });
  }, [route, gpx, setGpx]);

  const onRouteReplace = useCallback(
    (i: number) => {
      mapSetRoute(simplify(gpx.list[i].latlngs));

      setGpx({ list: gpx.list.map((el, index) => (i !== index ? el : { ...el, enabled: false })) });
    },
    [gpx, mapSetRoute, setGpx],
  );

  return (
    <div className="control-dialog control-dialog__left control-dialog__small">
      <div className="gpx-title">
        <div className="flex_1 big white upper">Tracks</div>
        <Switch active={gpx.enabled} onPress={toggleGpx} />
      </div>

      {gpx.list.map((item, index) => (
        <GpxDialogRow
          item={item}
          key={item.id}
          index={index}
          enabled={gpx.enabled}
          onRouteDrop={onRouteDrop}
          onFocusRoute={onFocusRoute}
          onRouteToggle={onRouteToggle}
          onRouteColor={onRouteColor}
          onRouteReplace={onRouteReplace}
        />
      ))}

      <div className="gpx-buttons">
        <button className="button outline">
          <input type="file" onChange={onGpxUpload} />
          Upload GPX
        </button>

        <div className={classNames('button outline', { disabled: !route.length })} onClick={addCurrent}>
          Add current
        </div>

        <div className={classNames('button success', { disabled: !route.length })} onClick={getGPXTrack}>
          Download current
        </div>
      </div>
    </div>
  );
};

export const GpxDialog = connect(mapStateToProps, mapDispatchToProps)(GpxDialogUnconnected);
