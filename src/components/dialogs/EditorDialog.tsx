import { createElement } from 'react';
import { connect } from 'react-redux';

import { modes } from '../../constants/modes';
import { selectEditorMode } from '../../redux/editor/selectors';
import { CancelDialog } from './CancelDialog';
import { GpxDialog } from './GpxDialog';
import { PolylineDialog } from './PolylineDialog';
import { ProviderDialog } from './ProviderDialog';
import { RouterDialog } from './RouterDialog';
import { SaveDialog } from './SaveDialog';
import { ShotPrefetchDialog } from './ShotPrefetchDialog';
import { TrashDialog } from './TrashDialog';

const mapStateToProps = (state) => ({ mode: selectEditorMode(state) });
type Props = ReturnType<typeof mapStateToProps> & { width: number };

const DIALOG_CONTENTS: Record<string, any> = {
  [modes.ROUTER]: RouterDialog,
  [modes.TRASH]: TrashDialog,
  [modes.SAVE]: SaveDialog,
  [modes.CONFIRM_CANCEL]: CancelDialog,
  [modes.PROVIDER]: ProviderDialog,
  [modes.SHOT_PREFETCH]: ShotPrefetchDialog,
  [modes.POLY]: PolylineDialog,
  [modes.GPX]: GpxDialog,
};

const EditorDialogUnconnected = (props: Props) => {
  return props.mode && DIALOG_CONTENTS[props.mode] ? createElement(DIALOG_CONTENTS[props.mode]) : null;
};

export const EditorDialog = connect(mapStateToProps)(EditorDialogUnconnected);
