import classNames from 'classnames';
import { FC, useCallback } from 'react';
import { connect } from 'react-redux';

import { setDirection } from '../../redux/editor/actions';
import { drawingDirections } from '../../redux/editor/constants';
import { selectEditorDirection } from '../../redux/editor/selectors';
import { mapSetRoute } from '../../redux/map/actions';
import { selectMapRoute } from '../../redux/map/selectors';
import { ReduxState } from '../../redux/store';
import { Icon } from '../panels';

const mapStateToProps = (state: ReduxState) => ({
  route: selectMapRoute(state),
  direction: selectEditorDirection(state),
});
const mapDispatchToProps = { mapSetRoute, setDirection };
type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

const PolylineDialogUnconnected: FC<Props> = ({ route, direction, setDirection, mapSetRoute }) => {
  const reverseRoute = useCallback(() => {
    if (route.length < 2) {
      return;
    }
    mapSetRoute([...route].reverse());
  }, [mapSetRoute, route]);

  const curRouteStart = useCallback(() => {
    if (route.length < 1) {
      return;
    }

    mapSetRoute(route.slice(1, route.length));
  }, [mapSetRoute, route]);

  const curRouteEnd = useCallback(() => {
    if (route.length < 1) {
      return;
    }

    mapSetRoute(route.slice(0, route.length - 1));
  }, [mapSetRoute, route]);

  const continueBackward = useCallback(() => {
    setDirection(drawingDirections.BACKWARDS);
  }, [setDirection]);

  const continueForward = useCallback(() => {
    setDirection(drawingDirections.FORWARDS);
  }, [setDirection]);

  return (
    <div className="control-dialog control-dialog__medium">
      <div className="helper">
        <div className="helper__text">
          <button className={classNames('helper__icon_button', { inactive: route.length < 2 })} onClick={reverseRoute}>
            <Icon icon="icon-reverse" />
          </button>

          <button className={classNames('helper__icon_button', { inactive: route.length < 1 })} onClick={curRouteStart}>
            <Icon icon="icon-drop-start" />
          </button>

          <button className={classNames('helper__icon_button', { inactive: route.length < 1 })} onClick={curRouteEnd}>
            <Icon icon="icon-drop-end" />
          </button>

          <button
            className={classNames('helper__icon_button', {
              inactive: route.length < 2,
              active: direction === drawingDirections.BACKWARDS,
            })}
            onClick={continueBackward}
          >
            <Icon icon="icon-draw-backward" />
          </button>

          <button
            className={classNames('helper__icon_button', {
              inactive: route.length < 2,
              active: direction === drawingDirections.FORWARDS,
            })}
            onClick={continueForward}
          >
            <Icon icon="icon-draw-forward" />
          </button>

          <div className="flex_1" />

          <div className="big white upper">Manual mode</div>
        </div>
      </div>
    </div>
  );
};

export const PolylineDialog = connect(mapStateToProps, mapDispatchToProps)(PolylineDialogUnconnected);
