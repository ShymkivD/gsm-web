import React from 'react';
import { connect } from 'react-redux';

import { modes } from '../../constants/modes';
import { changeMode, stopEditing } from '../../redux/editor/actions';
import { Icon } from '../panels';

const mapStateToProps = () => ({});
const mapDispatchToProps = { changeMode, stopEditing };

type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

class CancelDialogUnconnected extends React.Component<Props, any> {
  cancel = () => {
    this.props.stopEditing();
  };

  proceed = () => {
    this.props.changeMode(modes.NONE);
  };

  render() {
    return (
      <div className="control-dialog control-dialog__medium">
        <div className="helper cancel-helper">
          <div className="helper__text danger">
            <Icon icon="icon-cancel-1" />
            <div className="big upper">Close editor?</div>
          </div>
        </div>
        <div className="helper cancel-helper">
          <div className="helper__text" />
          <div className="helper__buttons">
            <div className="button router-helper__button" onClick={this.cancel}>
              Delete changes
            </div>

            <div className="button primary router-helper__button" onClick={this.proceed}>
              Return
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export const CancelDialog = connect(mapStateToProps, mapDispatchToProps)(CancelDialogUnconnected);
