export const AppInfoDialog = () => (
  <>
    <div style={{ flex: 1 }} />

    <div className="dialog-content">
      <div className="dialog-head">
        <div className="dialog-head-title">GSM Tracker</div>

        <hr />
        <div className="small app-info-list">
          <div>
            <div>Source code:</div>
            <a href="//github.com/muerwre/orchid-front" target="_blank" rel="noreferrer">
              github.com/muerwre/orchid-front
            </a>
            <br />
            <a href="//github.com/muerwre/orchid-backend" target="_blank" rel="noreferrer">
              github.com/muerwre/orchid-backend
            </a>
          </div>
          <div>
            <div>Frontend:</div>
            <a href="//reactjs.org/" target="_blank" rel="noreferrer">
              ReactJS
            </a>
            ,{' '}
            <a href="//leafletjs.com" target="_blank" rel="noreferrer">
              Leaflet
            </a>
            ,{' '}
            <a href="//www.liedman.net/leaflet-routing-machine/" target="_blank" rel="noreferrer">
              Leaflet Routing Machine
            </a>{' '}
          </div>
          <div>
            <div>Backend:</div>
            <a href="//project-osrm.org/" target="_blank" rel="noreferrer">
              OSRM
            </a>
            ,{' '}
            <a href="//golang.org/" target="_blank" rel="noreferrer">
              Golang
            </a>
            ,{' '}
            <a href="//nginx.org/" target="_blank" rel="noreferrer">
              Nginx
            </a>
            ,{' '}
          </div>
        </div>
      </div>
    </div>
  </>
);
