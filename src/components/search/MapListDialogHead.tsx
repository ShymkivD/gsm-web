import Range from 'rc-slider/lib/Range';
import { ChangeEventHandler, FC, memo } from 'react';

interface Props {
  ready: boolean;
  min: number;
  max: number;
  search: string;
  distance: [number, number];
  onDistanceChange: (val: [number, number]) => void;
  onSearchChange: ChangeEventHandler<HTMLInputElement>;
}

export const MapListDialogHead: FC<Props> = memo(
  ({ min, max, ready, distance, search, onSearchChange, onDistanceChange }) => {
    const marks = [...new Array(11)].reduce(
      (obj, _, i) => ({ ...obj, [i * 40]: i * 40 < 400 ? ` ${i * 40}` : ` ${i * 40}+` }),
      {},
    );

    return (
      <div className="dialog-head">
        <div>
          <input type="text" placeholder="Search by title" value={search} onChange={onSearchChange} />

          <div />

          {ready && Object.keys(marks).length > 2 ? (
            <Range
              min={0}
              max={400}
              marks={marks}
              step={50}
              onChange={onDistanceChange}
              defaultValue={[0, 10000]}
              value={distance}
              pushable
              disabled={min >= max}
            />
          ) : (
            <div className="range-placeholder" />
          )}
        </div>
      </div>
    );
  },
);
