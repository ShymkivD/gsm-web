import classnames from 'classnames';
import { FC, memo } from 'react';

import { GpxRoute } from '../../redux/editor';
import { Icon } from '../panels/Icon';
import { Switch } from '../Switch';

type Props = {
  item: GpxRoute;
  index: number;
  enabled: boolean;

  onFocusRoute: (i: number) => void;
  onRouteDrop: (i: number) => void;
  onRouteToggle: (i: number) => void;
  onRouteColor: (i: number) => void;
  onRouteReplace: (i: number) => void;
};

export const GpxDialogRow: FC<Props> = memo(
  ({ item, index, enabled, onRouteToggle, onFocusRoute, onRouteDrop, onRouteColor, onRouteReplace }) => (
    <div className={classnames('gpx-row', { 'gpx-row_disabled': !enabled || !item.enabled })}>
      <div className="gpx-row__color" style={{ backgroundColor: item.color }} onClick={() => onRouteColor(index)} />

      <div className="gpx-row__title" onClick={() => onFocusRoute(index)}>
        {item.name}
      </div>

      <div className="gpx-row__buttons">
        <div onClick={() => onRouteReplace(index)}>
          <Icon icon="icon-to-poly" size={24} />
        </div>

        <div onClick={() => onRouteDrop(index)}>
          <Icon icon="icon-trash-6" size={24} />
        </div>

        <div>
          <Switch active={item.enabled} onPress={() => onRouteToggle(index)} />
        </div>
      </div>
    </div>
  ),
);
