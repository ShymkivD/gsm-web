import { memo } from 'react';

import { UserLocation } from '../UserLocation';
import { DistanceBar } from './DistanceBar';

export const TopLeftPanel = memo(() => (
  <div className="status-panel top left">
    <UserLocation />
    <DistanceBar />
  </div>
));
