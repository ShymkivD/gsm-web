import { useCallback } from 'react';
import { connect } from 'react-redux';

import { modes } from '../../constants/modes';
import { tileMaps } from '../../constants/providers';
import { changeMode } from '../../redux/editor/actions';
import { selectEditor } from '../../redux/editor/selectors';
import { selectMap } from '../../redux/map/selectors';
import { ReduxState } from '../../redux/store';
import { Icon } from './Icon';
import { Tooltip } from './Tooltip';

const mapStateToProps = (state: ReduxState) => {
  const { provider } = selectMap(state);
  const { markers_shown, editing } = selectEditor(state);

  return { provider, markers_shown, editing };
};
const mapDispatchToProps = { changeMode };
type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

const TopRightPanelUnconnected = ({ provider, changeMode }: Props) => {
  const startProviderMode = useCallback(() => changeMode(modes.PROVIDER), [changeMode]);
  const clearMode = useCallback(() => changeMode(modes.NONE), [changeMode]);

  return (
    <div className="status-panel top right">
      <div
        className="status-bar pointer top-control padded tooltip-container"
        onClick={startProviderMode}
        onBlur={clearMode}
        tabIndex={-1}
      >
        <Tooltip position="top">Map style</Tooltip>
        <Icon icon="icon-map-1" size={24} />
        <div className="status-bar-sep" />
        <span>{(provider && tileMaps[provider] && tileMaps[provider].name) || 'Map style'}</span>
      </div>
    </div>
  );
};

export const TopRightPanel = connect(mapStateToProps, mapDispatchToProps)(TopRightPanelUnconnected);
