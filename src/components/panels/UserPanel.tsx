import classnames from 'classnames';
import { PureComponent } from 'react';
import { connect } from 'react-redux';

import { api } from '../../constants/api';
import { defaultUser, Role } from '../../constants/auth';
import { Dialogs, Tabs } from '../../constants/dialogs';
import { modes } from '../../constants/modes';
import { changeMode, getGPXTrack, setDialog, setDialogActive, takeAShot } from '../../redux/editor/actions';
import { ReduxState } from '../../redux/store';
import { gotOAuthUser, openMapDialog, setUser, userLogout } from '../../redux/user/actions';
import { TitleDialog } from '../dialogs';
import { GuestButton, UserButton, UserMenu } from '../user';
import { Icon } from './Icon';
import { Tooltip } from './Tooltip';

const mapStateToProps = ({
  user: { user },
  editor: { dialog, dialog_active, features },
  map: { route },
}: ReduxState) => ({ dialog, dialog_active, user, route, features });
const mapDispatchToProps = {
  setUser,
  userLogout,
  takeAShot,
  setDialog,
  gotOAuthUser,
  setDialogActive,
  openMapDialog,
  getGPXTrack,
  changeMode,
};
type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;
type State = { menuOpened: boolean };

export class UserPanelUnconnected extends PureComponent<Props, State> {
  state = { menuOpened: false };

  componentDidMount() {
    window.addEventListener('message', (e) => {
      const {
        data: { type, data },
      } = e;

      if (!data || !['error', 'oauth_login'].includes(type)) {
        return;
      }

      if (type === 'error') {
        // TODO: handle error
        console.log(`Error from OAuth window`, data.message);
        return;
      }

      if (!data?.id || !data.token) {
        return;
      }

      const { id, token, role = 'user', name = '', ip = '', photo = '', agent = '' } = data;
      const user = { ...defaultUser, role, id, token, name, ip, agent, photo };

      this.setState({ menuOpened: false });
      this.props.gotOAuthUser(user);
    });
  }

  setMenuOpened = () => this.setState({ menuOpened: !this.state.menuOpened });

  openMapsDialog = () => {
    this.props.openMapDialog(Tabs.MY);
  };

  openAppInfoDialog = () => {
    this.setMenuOpened();
    this.props.setDialog(Dialogs.APP_INFO);
    this.props.setDialogActive(this.props.dialog !== Dialogs.APP_INFO);
  };

  openOauthFrame = () => {
    const width = parseInt(String(window.innerWidth), 10);
    const height = parseInt(String(window.innerHeight), 10);
    const top = (height - 370) / 2;
    const left = (width - 700) / 2;

    window.open(
      api.iframeLogin,
      'socialPopupWindow',
      `location=no,width=650,height=750,toolbar=no,menubar=no,scrollbars=no,top=${top},left=${left},resizable=no`,
    );
  };

  openGpxDialog = () => {
    this.props.changeMode(modes.GPX);
  };

  render() {
    const {
      props: { user, dialog, dialog_active },
      state: { menuOpened },
    } = this;

    return (
      <div>
        <TitleDialog />

        <div className="panel active panel-user">
          <div className="user-panel">
            {!user || user.role === Role.guest ? (
              <GuestButton onClick={this.openOauthFrame} />
            ) : (
              <UserButton user={user} setMenuOpened={this.setMenuOpened} />
            )}
            {user?.role !== 'guest' && menuOpened && (
              <UserMenu userLogout={this.props.userLogout} openAppInfoDialog={this.openAppInfoDialog} />
            )}
          </div>

          <div className="control-sep" />

          <div className="control-bar">
            <button
              className={classnames({
                active: dialog_active && dialog === Dialogs.MAP_LIST,
              })}
              onClick={this.openMapsDialog}
            >
              <Tooltip>Routes catalogue</Tooltip>
              <Icon icon="icon-folder-1" />
            </button>
          </div>

          <>
            <div className="control-sep" />

            <div className="control-bar">
              <button onClick={this.openGpxDialog}>
                <Tooltip>Export GPX</Tooltip>
                <Icon icon="icon-gpx-1" />
              </button>
            </div>
          </>

          <div className="control-sep" />

          <div className="control-bar">
            <button className={classnames({ active: false })} onClick={this.props.takeAShot}>
              <Tooltip>Snapshot of map</Tooltip>
              <Icon icon="icon-shot-4" />
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export const UserPanel = connect(mapStateToProps, mapDispatchToProps)(UserPanelUnconnected);
