import pick from 'ramda/es/pick';
import Slider from 'rc-slider/lib/Slider';
import React from 'react';
import { connect } from 'react-redux';

import { setSpeed } from '../../redux/editor/actions';
import { selectEditor } from '../../redux/editor/selectors';
import { ReduxState } from '../../redux/store';
import { toHours } from '../../utils/format';
import { isMobile } from '../../utils/window';
import { Icon } from './Icon';
import { Tooltip } from './Tooltip';

const mapStateToProps = (state: ReduxState) => pick(['distance', 'estimated', 'speed'], selectEditor(state));
const mapDispatchToProps = { setSpeed };
type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

interface State {
  dialogOpened: boolean;
}

class DistanceBarUnconnected extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
    this.state = { dialogOpened: false };
  }

  step = 15;
  min = 20;
  max = 110;

  marks: { [x: number]: string } = [...Array(Math.floor(this.max - this.min) / this.step + 1)].reduce(
    (obj, el, index) => ({ ...obj, [this.min + index * this.step]: String(this.min + index * this.step) }),
    {},
  );

  toggleDialog = () => {
    if (isMobile()) {
      return;
    }

    this.setState({ dialogOpened: !this.state.dialogOpened });
  };

  render() {
    const {
      props: { distance, estimated, speed },
      state: { dialogOpened },
      min,
      max,
      step,
      marks,
    } = this;

    return (
      <>
        <div className="status-bar padded pointer tooltip-container" onClick={this.toggleDialog}>
          {distance} km&nbsp;
          <Tooltip position="top">Approximate time</Tooltip>
          <span className="desktop-only">
            <Icon icon="icon-marker" size={28} />
          </span>
          <div className="desktop-only">{toHours(estimated)}&nbsp;hours&nbsp;</div>
        </div>
        {dialogOpened && (
          <div className="control-dialog top left" style={{ left: 0, top: 42 }}>
            <div className="helper speed-helper" onBlur={() => this.setState({ dialogOpened: false })}>
              <Slider
                min={min}
                max={max}
                step={step}
                onChange={this.props.setSpeed}
                defaultValue={15}
                value={speed}
                marks={marks}
              />
            </div>
          </div>
        )}
      </>
    );
  }
}

export const DistanceBar = connect(mapStateToProps, mapDispatchToProps)(DistanceBarUnconnected);
