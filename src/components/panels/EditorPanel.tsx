import classnames from 'classnames';
import { PureComponent } from 'react';
import { connect } from 'react-redux';

import { modes } from '../../constants/modes';
import { changeMode, keyPressed, redo, startEditing, stopEditing, takeAShot, undo } from '../../redux/editor/actions';
import { selectEditor } from '../../redux/editor/selectors';
import { selectMap } from '../../redux/map/selectors';
import { ReduxState } from '../../redux/store';
import { EditorDialog } from '../dialogs';
import { Icon } from './Icon';
import { Tooltip } from './Tooltip';

const mapStateToProps = (state: ReduxState) => {
  const { mode, changed, editing, features, history } = selectEditor(state);
  const { route } = selectMap(state);
  return { mode, changed, editing, features, history, route };
};
const mapDispatchToProps = { changeMode, startEditing, stopEditing, takeAShot, keyPressed, undo, redo };
type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

class EditorPanelUnconnected extends PureComponent<Props, any> {
  componentDidMount() {
    window.addEventListener('keydown', this.onKeyPress as any);

    const obj = document.getElementById('control-dialog');
    const { width } = this.panel.getBoundingClientRect();

    if (!this.panel || !obj) {
      return;
    }

    obj.style.width = String(width);
  }

  panel: HTMLElement = null;

  componentWillUnmount() {
    window.removeEventListener('keydown', this.onKeyPress as any);
  }

  onKeyPress = (event: Event & { key: string; target: { tagName: string } }) => {
    if (event.target.tagName === 'TEXTAREA' || event.target.tagName === 'INPUT') {
      return;
    }

    this.props.keyPressed(event);
  };

  startPolyMode = () => this.props.changeMode(modes.POLY);
  startRouterMode = () => this.props.changeMode(modes.ROUTER);
  startTrashMode = () => this.props.changeMode(modes.TRASH);
  startSaveMode = () => this.props.changeMode(modes.SAVE);

  render() {
    const {
      mode,
      changed,
      editing,
      features: { routing },
      history: { records, position },
      route,
    } = this.props;

    const can_undo = records.length > 0 && position > 0;
    const can_redo = records.length && records.length - 1 > position;
    const can_clear = route.length > 0;

    return (
      <div>
        <div
          className={classnames('panel right', { active: editing })}
          ref={(el) => {
            this.panel = el;
          }}
        >
          <div
            className={classnames('secondary-bar secondary-bar__undo', {
              active: can_undo || can_redo || can_clear,
            })}
          >
            <button className={classnames({ inactive: !can_undo })} onClick={this.props.undo}>
              <Tooltip>Undo (z)</Tooltip>
              <Icon icon="icon-undo" size={24} />
            </button>

            <button className={classnames({ inactive: !can_redo })} onClick={this.props.redo}>
              <Tooltip>Return (x)</Tooltip>
              <Icon icon="icon-redo" size={24} />
            </button>

            <button className={classnames({ inactive: !can_clear })} onClick={this.startTrashMode}>
              <Tooltip>Clear (c)</Tooltip>
              <Icon icon="icon-trash-4" size={24} />
            </button>
          </div>

          <div className="control-bar control-bar-padded">
            {routing && (
              <button className={classnames({ active: mode === modes.ROUTER })} onClick={this.startRouterMode}>
                <Tooltip>Automatic route</Tooltip>
                <Icon icon="icon-route-2" />
              </button>
            )}

            <button className={classnames({ active: mode === modes.POLY })} onClick={this.startPolyMode}>
              <Tooltip>Edit route</Tooltip>
              <Icon icon="icon-poly-3" />
            </button>
          </div>

          <div className="control-sep" />

          <div className="control-bar">
            <button className="highlighted cancel" onClick={this.props.stopEditing}>
              <Icon icon="icon-cancel-1" />
            </button>

            <button className={classnames({ primary: changed, inactive: !changed })} onClick={this.startSaveMode}>
              <span className="desktop-only">SAVE</span>
              <Icon icon="icon-check-1" />
            </button>
          </div>
        </div>

        <div className={classnames('panel right', { active: !editing })}>
          <div className="control-bar">
            <button className="primary single" onClick={this.props.startEditing}>
              <Icon icon="icon-route-2" />
              <span>EDIT</span>
            </button>
          </div>
        </div>

        <EditorDialog width={this.panel?.getBoundingClientRect().width || 0} />
      </div>
    );
  }
}

export const EditorPanel = connect(mapStateToProps, mapDispatchToProps)(EditorPanelUnconnected);
