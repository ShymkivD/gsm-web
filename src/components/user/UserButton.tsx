import { FC, memo } from 'react';

import { User } from '../../constants/auth';
import { UserPicture } from './UserPicture';

interface Props {
  user: User;
  setMenuOpened: () => void;
}

export const UserButton: FC<Props> = memo(({ setMenuOpened, user: { uid, photo, name } }) => (
  <div className="control-bar user-bar">
    <div className="user-button" onClick={setMenuOpened}>
      <UserPicture photo={photo} />

      <div className="user-button-fields">
        <div className="user-button-name">{name || uid || '...'}</div>
        <div className="user-button-text">{uid || 'User'}</div>
      </div>
    </div>
  </div>
));
