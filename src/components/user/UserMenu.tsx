import { userLogout } from '../../redux/user/actions';

interface Props {
  userLogout: typeof userLogout;
  openAppInfoDialog: () => void;
}

export const UserMenu = ({ userLogout, openAppInfoDialog }: Props) => (
  <div className="user-panel-menu">
    <div className="user-panel-title">GSM TRACKER</div>
    <div className="user-panel-item" onClick={openAppInfoDialog}>
      About application
    </div>
    <div className="user-panel-item" onClick={userLogout}>
      Logout
    </div>
  </div>
);
