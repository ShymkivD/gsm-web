import { connect } from 'react-redux';

import { Fills } from '../components/Fills';
import { EditorPanel, TopLeftPanel, TopRightPanel, UserPanel } from '../components/panels';
import { Renderer } from '../components/renderer/Renderer';
import { Dialogs } from '../constants/dialogs';
import { Map } from '../map/Map';
import { EditorState } from '../redux/editor';
import { hideRenderer, setDialogActive } from '../redux/editor/actions';
import { ReduxState } from '../redux/store';
import { LeftDialog } from './LeftDialog';

type Props = {
  renderer_active: boolean;

  mode: EditorState['mode'];
  dialog: keyof typeof Dialogs;
  dialog_active: boolean;
  hideRenderer: typeof hideRenderer;
  setDialogActive: typeof setDialogActive;
};

const AppUnconnected = (props: Props) => (
  <div>
    <Fills />
    <UserPanel />
    <EditorPanel />

    <TopLeftPanel />
    <TopRightPanel />

    <LeftDialog dialog={props.dialog} dialog_active={props.dialog_active} setDialogActive={props.setDialogActive} />

    <Map />

    {props.renderer_active && <Renderer onClick={props.hideRenderer} />}
  </div>
);

const mapStateToProps = ({ editor: { mode, dialog, dialog_active, renderer } }: ReduxState) => ({
  renderer_active: renderer.renderer_active,
  mode,
  dialog,
  dialog_active,
});

const mapDispatchToProps = { hideRenderer, setDialogActive };

export const App = connect(mapStateToProps, mapDispatchToProps)(AppUnconnected);
