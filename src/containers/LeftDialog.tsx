import classnames from 'classnames';
import { createElement, FC, memo } from 'react';

import { AppInfoDialog } from '../components/dialogs/AppInfoDialog';
import { MapListDialog } from '../components/dialogs/MapListDialog';
import { Icon } from '../components/panels/Icon';
import { Dialogs } from '../constants/dialogs';
import { setDialogActive } from '../redux/editor/actions';

interface Props {
  dialog: keyof typeof Dialogs;
  dialog_active: boolean;
  setDialogActive: typeof setDialogActive;
}

const LEFT_DIALOGS = {
  [Dialogs.MAP_LIST]: MapListDialog,
  [Dialogs.APP_INFO]: AppInfoDialog,
};

export const LeftDialog: FC<Props> = memo(({ dialog, dialog_active, setDialogActive }) => (
  <>
    {Object.keys(LEFT_DIALOGS).map((item) => (
      <div className={classnames('dialog', { active: dialog_active && dialog === item })} key={item}>
        {dialog && LEFT_DIALOGS[item] && createElement(LEFT_DIALOGS[item], {})}

        <div className="dialog-close-button desktop-only" onClick={() => setDialogActive(false)}>
          <Icon icon="icon-cancel-1" />
        </div>

        <div className="dialog-close-button mobile-only" onClick={() => setDialogActive(false)}>
          <Icon icon="icon-chevron-down" />
        </div>
      </div>
    ))}
  </>
));
