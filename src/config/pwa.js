import { join } from 'path';

export function MANIFEST(src) {
  return {
    name: 'GSM Tracker',
    short_name: 'Routes',
    description: 'App for tracking you moves',
    background_color: '#333333',
    theme_color: '#01579b',
    display: 'fullscreen',
    'theme-color': '#01579b',
    start_url: '/',
    icons: [
      {
        src,
        sizes: [96, 128, 192, 256, 384, 512],
        destination: join('assets', 'icons'),
      },
    ],
  };
}
export const PUBLIC_PATH = process.env.REACT_APP_PUBLIC_PATH;
