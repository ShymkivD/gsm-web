import { LatLngLiteral } from 'leaflet';

import { defaultMapProvider, tileMaps } from '../constants/providers';

const publicPath = process.env.REACT_APP_PUBLIC_PATH || '';
const apiAddress = process.env.REACT_APP_API_ADDR || '';
const osrmUrl = process.env.REACT_APP_OSRM_URL || '';
const osrmProfile = process.env.REACT_APP_OSRM_PROFILE || 'car';
const osrmTestUrl = ([south_west, north_east]: LatLngLiteral[]) =>
  `${osrmUrl}/${osrmProfile}/${Object.values(south_west).join(',')};${Object.values(north_east).join(',')}`;

export const client = { osrmUrl, apiAddress, osrmTestUrl, osrmProfile, strokeWidth: 5, publicPath };

export const colors = { path: ['#ff7700', '#ff3344'] };

export const provider = tileMaps[defaultMapProvider];

export const mobileBreakpoint = 768;
