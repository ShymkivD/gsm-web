import { client } from '../config/frontend';

export const api = {
  getGuest: `${client.apiAddress}/auth`,
  checkToken: `${client.apiAddress}/auth`,
  iframeLogin: `${client.apiAddress}/auth/github`,
  getMap: `${client.apiAddress}/route`,
  postMap: `${client.apiAddress}/route`,
  getRouteList: (tab) => `${client.apiAddress}/route/list/${tab}`,

  dropRoute: `${client.apiAddress}/route`,
  modifyRoute: `${client.apiAddress}/route`,
  setStarred: `${client.apiAddress}/route/publish`,
};

export const apiRetryInterval = 10;
