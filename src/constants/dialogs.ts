export const Dialogs = {
  NONE: 'NONE',
  MAP_LIST: 'MAP_LIST',
  APP_INFO: 'APP_INFO',
};

export const Tabs = {
  MY: 'my',
  PENDING: 'pending',
  STARRED: 'starred',
};

export const TabTitles = {
  [Tabs.MY]: 'My',
  [Tabs.PENDING]: 'Applications',
  [Tabs.STARRED]: 'Catalog',
};
