import { FeatureGroup, FitBoundsOptions, Map } from 'leaflet';

export class MapContainer extends Map {
  constructor(props) {
    super(props);
    this.routeLayer.addTo(this);
  }

  disableClicks = () => {
    this.clickable = false;
  };

  enableClicks = () => {
    this.clickable = true;
  };

  getVisibleBounds = () => {
    const layers = [this.routeLayer];
    for (let i = 0; i < layers.length; i += 1) {
      const bounds = layers[i].getBounds();
      if (Object.keys(bounds).length == 2) {
        return bounds;
      }
    }
    return null;
  };

  fitVisibleBounds = (options: FitBoundsOptions) => {
    const bounds = this.getVisibleBounds();

    if (!bounds) {
      return;
    }
    this.fitBounds(bounds, options);
  };

  public clickable = true;

  public routeLayer = new FeatureGroup();
}

export const MainMap = new MapContainer(document.getElementById('canvas')).setView(
  // TODO: dynamic user location or ??
  [50.4364397, 30.5613589],
  13,
);
