export const Tips = {
  SAVE_INFO: 'No one but you can change the route - only create a copy of it and save it at another address',
  SAVE_TIMED_OUT: `Server didn't respond, try again`,
  SAVE_EMPTY: 'This route is empty, draw something for starters',
  SAVE_SUCCESS: 'Great! Your route is saved. Share the link with your friends, have a nice ride!',
  SAVE_OVERWRITE: 'You already have a route with this name. Can you overwrite it or rename it?',
  SAVE_EXISTS: 'There is already a route with this address. Think of something else.',
};
