export interface User {
  new_messages: number;
  place_types: any;
  random_url: string;
  role: Role;
  routes: any;
  success: boolean;
  id?: string;
  uid: string;
  token?: string;
  photo: string;
  name: string;
}

export enum Role {
  guest = 'guest',
  user = 'user',
  admin = 'admin',
}

export const defaultUser: User = {
  new_messages: 0,
  place_types: {},
  random_url: '',
  role: Role.guest,
  routes: {},
  success: false,
  id: null,
  token: null,
  photo: null,
  name: null,
  uid: null,
};
