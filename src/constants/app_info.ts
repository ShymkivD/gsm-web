export const AppInfo = {
  VERSION: 1,
  RELEASE: 1,
  CHANGELOG: {
    1: [['Cloned from repo https://github.com/muerwre/orchidMap']],
    2: [['Rewrite backend in Nest.js']],
  },
};
