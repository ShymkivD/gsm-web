export interface MapProvider {
  name: string;
  url: string;
  range: Array<string | number>;
}

export const tileMaps: Record<string, MapProvider> = {
  DEFAULT: {
    name: 'OpenStreetMap',
    url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    range: ['a', 'b', 'c'],
  },
  DGIS: {
    name: '2gis',
    url: 'https://tile1.maps.2gis.com/tiles?x={x}&y={y}&z={z}&v=1',
    range: ['a', 'b', 'c'],
  },
  ESAT: {
    name: 'Sattelite',
    url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
    range: [1, 2, 3, 4],
  },
};

const mapProviders = Object.keys(tileMaps);
export const defaultMapProvider = mapProviders[0];

export const replaceProviderUrl = (provider, { x, y, zoom }: { x: number; y: number; zoom: number }): string => {
  const { url, range } = tileMaps[provider] || tileMaps[defaultMapProvider];
  const random: number | string =
    range && range.length >= 2 ? range[Math.round(Math.random() * (range.length - 1))] : 1;

  return url
    .replace('{x}', String(x))
    .replace('{y}', String(y))
    .replace('{z}', String(zoom))
    .replace('{s}', String(random));
};
