module.exports = {
  webpack: {
    output: {
      publicPath: '/',
    },
  },
  eslint: {
    enable: true,
    mode: 'file',
  },
};
